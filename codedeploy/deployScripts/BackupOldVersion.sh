#!/bin/bash
if [ -d /var/www/html/app ]; then
  cp /var/www/html/app/*.txt /tmp
  mv /var/www/html/app /tmp/app_old
fi
if [ -e /tmp/deployVersion.txt ]; then
  mv /tmp/deployVersion.txt /tmp/deployVersion_old.txt
fi

