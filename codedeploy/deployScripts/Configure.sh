#!/bin/bash

DEPLOY_VERSION=`cat /tmp/deployVersion.txt`
cp /tmp/apiurl.txt /var/www/html/app
if [ -f /tmp/env-type.txt ]; then
  cp /tmp/env-type.txt /var/www/html/app
fi
chown root:apache /var/www/html/app/*.txt
chown root:apache /var/www/html/app

sed -i "s/_VERSION/${DEPLOY_VERSION}/g" /var/www/html/app/components/mainNavigation/mainNavigation.template.html
sed -i "s/_VERSION/${DEPLOY_VERSION}/g" /var/www/html/app/index.html