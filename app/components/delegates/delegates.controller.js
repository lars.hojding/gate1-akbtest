angular.module('app.controllers')
.controller('DelegatesCtrl', ['$scope', 'Delegates', '$stateParams', 'Feedback', 'WaitLock',
            function ($scope, Delegates, $stateParams, Feedback, WaitLock){
    $scope.orgNr = $stateParams.orgNr;

    $scope.delegates = [];

    $scope.editing = false;
    var editOrigTmp = null;
    $scope.edit = function(delegate){
        if($scope.editing)return;
        delegate.editing = $scope.editing = true;
        editOrigTmp = angular.copy(delegate);
    };
    var quitEditing = function(delegate, form){
        editOrigTmp = null;
        delegate.editing = $scope.editing = false;
        form.$setPristine();
        form.$setUntouched();
    };
    $scope.undoEdit = function(delegate, form){
        if(WaitLock.check())return;
        if(delegate.editing){
            angular.copy(editOrigTmp, delegate);
            quitEditing(delegate, form);
        }
    };

    var loadDelegatesList = function(){
        Feedback.http('delegates.get', Delegates.getDelegates($scope.orgNr), {
            403: 'Ej behörig att se uppgifter för företagets användare',
            '4XX': 'Information om företagets användare kunde inte hämtas',
            '5XX': 'Ett fel inträffade. Information om företagets användare kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.delegates = res.data;
        });
    };
    loadDelegatesList();

    $scope.save = function(delegate, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('delegates.update', Delegates.updateDelegate(delegate), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra uppgifter för användare',
            '4XX': 'Uppgifterna för användaren kunde inte ändras',
            '5XX': 'Ett fel inträffade. Uppgifterna för användaren kunde inte ändras. Försök igen senare.'
        })
        .then(function(){
            quitEditing(delegate, form);
        })
        ['finally'](function(){
            WaitLock.unlock();
            $scope.editing = false;
            var editOrigTmp = null;
            loadDelegatesList();
        });
    };

    $scope.remove = function(delegate){
        if(WaitLock.check())return;
        Feedback.http('delegates.delete', Delegates.deleteDelegate(delegate), {
            403: 'Ej behörig att radera uppgifter för användare',
            '4XX': 'Uppgifterna för användaren kunde inte raderas',
            '5XX': 'Ett fel inträffade. Uppgifterna för användaren kunde inte raderas. Försök igen senare.'
        })
        ['finally'](function(){
            loadDelegatesList();
        });
    };
}]);
