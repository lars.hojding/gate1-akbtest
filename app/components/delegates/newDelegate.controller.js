angular.module('app.controllers')
.controller('NewDelegateCtrl', ['$scope', 'Delegates', '$stateParams', 'Feedback', '$state', 'WaitLock',
            function ($scope, Delegates, $stateParams, Feedback, $state, WaitLock){
    var orgNr = $stateParams.orgNr;

    $scope.delegate = {
        "pNr": "",
        "contactInfo": {
            "phoneNumber": "",
            "email": ""
        }
    };

    $scope.save = function(){
        $scope.delegate.orgNr = orgNr;
        var form = $scope.delegateForm;
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('delegates.create').add('delegates.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla fälten');
            }else{
                Feedback.clearTarget('delegates.create').add('delegates.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('delegates.create', Delegates.createDelegate($scope.delegate), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till användare',
            409: 'Användaren är redan tillagd',
            '4XX': 'Användaren kunde inte läggas till',
            '5XX': 'Ett fel inträffade. Användaren kunde inte läggas till. Försök igen senare.'
        })
        .then(function(){
            $state.go('user.company.delegates.entries', {orgNr: orgNr});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };
}]);
