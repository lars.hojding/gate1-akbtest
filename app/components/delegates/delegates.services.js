angular.module('app.services')
.factory('Delegates', ['AuthHttp', function(AuthHttp){
    return {
        getDelegates: function(orgNr){
            return AuthHttp.get('delegates/', {params: {orgNr: orgNr}});
        },
        deleteDelegate: function(delegateData){
            return AuthHttp['delete']('delegates/', {params: {orgNr: delegateData.orgNr, pNr: delegateData.pNr}});
        },
        updateDelegate: function(delegateData){
            return AuthHttp.put('delegates/', delegateData);
        },
        createDelegate: function(delegateData){
            return AuthHttp.post('delegates/', delegateData);
        }
    }
}]);
