angular.module('app.directives')
.directive('svabOwnerid', ['OrgnrPersnrValidation', function(OrgnrPersnrValidation){
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl){
            var validator = function(viewValue){
                var c = OrgnrPersnrValidation(viewValue);
                var cases = [
                    'invalid_format',
                    '10digit_per',
                    'org_luhn',
                    '12digit_org',
                    'sam_luhn',
                    'per_date',
                    'per_luhn'
                ];
                if(c.valid){
                    ctrl.$setValidity('ownerid', true);
                    angular.forEach(cases, function(v){
                        ctrl.$setValidity('ownerid-' + v, true);
                    });
                    return c.formatted;
                }else{
                    ctrl.$setValidity('ownerid', false);
                    angular.forEach(cases, function(v){
                        if(c.error == v.toUpperCase()){
                            ctrl.$setValidity('ownerid-' + v, false);
                        }else{
                            ctrl.$setValidity('ownerid-' + v, true);
                        }
                    });
                    return;
                }
            };

            var validatorFactory = function(errorType){
                return function(modelValue, viewValue){
                    var c = OrgnrPersnrValidation(modelValue || viewValue);
                    if(typeof errorType === 'undefined'){
                        return c.valid;
                    }
                    return !(!c.valid && c.error == errorType.toUpperCase());
                };
            };

            ctrl.$validators['ownerid-invalid_format'] = validatorFactory('invalid_format');
            ctrl.$validators['ownerid-10digit_per'] = validatorFactory('10digit_per');
            ctrl.$validators['ownerid-org_luhn'] = validatorFactory('org_luhn');
            ctrl.$validators['ownerid-12digit_org'] = validatorFactory('12digit_org');
            ctrl.$validators['ownerid-sam_luhn'] = validatorFactory('sam_luhn');
            ctrl.$validators['ownerid-per_date'] = validatorFactory('per_date');
            ctrl.$validators['ownerid-per_luhn'] = validatorFactory('per_luhn');
            ctrl.$validators['ownerid'] = validatorFactory();

            ctrl.$parsers.unshift(function(value){
                var c = OrgnrPersnrValidation(value);
                return c.valid ? c.formatted : value;
            });
        }
    }
}]);
