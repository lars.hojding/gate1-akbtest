angular.module('app.directives')
.directive('svabGeqThan', function(){
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl){
            var validate = function(viewValue){
                ctrl.$setValidity('geq', parseInt(viewValue) >= parseInt(attrs.svabGeqThan));
                return viewValue;
            };
            ctrl.$parsers.unshift(validate);
            ctrl.$formatters.push(validate);

            attrs.$observe('svabGeqThan', function(m){
                return validate(ctrl.$viewValue);
            });
        }
    };
})
.directive('svabLeqThan', function(){
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl){
            var validate = function(viewValue){
                ctrl.$setValidity('leq', parseInt(viewValue) <= parseInt(attrs.svabLeqThan));
                return viewValue;
            };
            ctrl.$parsers.unshift(validate);
            ctrl.$formatters.push(validate);

            attrs.$observe('svabLeqThan', function(m){
                return validate(ctrl.$viewValue);
            });
        }
    };
});
