angular.module('app.directives')
.directive('existingOwner', ['$q', '$timeout', function($q, $timeout){
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl){
            ctrl.$asyncValidators.existingOwner = function(modelValue, viewValue){
                if(ctrl.$isEmpty(modelValue)){
                    return $q.when();
                }

                var deferred = $q.defer();

                $timeout(function(){
                    if(modelValue == '1212121212'){
                        deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                }, 3000);

                return deferred.promise;
            };
        }
    };
}]);