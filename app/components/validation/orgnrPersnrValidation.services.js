angular.module('app.services')
.factory('OrgnrPersnrValidation', [function(){
    return function(number){
        var checkedFormat = (function(viewValue){
            if(/^[0-9]{10}$/.test(viewValue)){
                return {
                    valid: true,
                    standardFormat: '' + viewValue,
                    appFormat: '' + viewValue,
                    type: 'o'
                };
            }else if(/^[0-9]{12}$/.test(viewValue)){
                if(/^(19|20)/.test(viewValue)){
                    return {
                        valid: true,
                        standardFormat: (''+viewValue).substr(2, 10),
                        appFormat: (''+viewValue).substr(0, 12),
                        type: 'p',
                        century: viewValue.substr(0,2)
                    };
                }else{
                    return {valid: false};
                }
            }else if(/^[0-9]{6}-[0-9]{4}$/.test(viewValue)){
                return {
                    valid: true,
                    standardFormat: (''+viewValue).substr(0, 6) + (''+viewValue).substr(7, 4),
                    appFormat: (''+viewValue).substr(0, 6) + (''+viewValue).substr(7, 4),
                    type: 'o'
                };
            }else if(/^[0-9]{8}-[0-9]{4}$/.test(viewValue)){
                if(/^(19|20)/.test(viewValue)){
                    return {
                        valid: true,
                        standardFormat: (''+viewValue).substr(2, 6) + (''+viewValue).substr(9, 4),
                        appFormat: (''+viewValue).substr(0, 8) + (''+viewValue).substr(9, 4),
                        type: 'p',
                        century: viewValue.substr(0,2)
                    };
                }else{
                    return {valid: false};
                }
            }else{
                return {valid: false};
            }
        })(number);

        var checkLuhn = function(viewValue){
            var sum = 0;
            for(var i = 0; i < 9; i++){
                var c = (2 - (i % 2)) * parseInt(('' + viewValue).substr(i, 1));
                if(c < 10){
                    sum += c;
                }else{
                    sum += c - 9;
                }
            }
            var checkDigit = 10 - parseInt(sum) % 10;
            if(checkDigit > 9)checkDigit -= 10;
            return parseInt(('' + viewValue).substr(9, 1)) === checkDigit;
        };

        var realDate = function(v){
            var d = new Date(v + 'T00:00:00Z');
            var valid = false;
            if(typeof d === 'object'){
                try{
                    var ds = d.toISOString();
                }catch(err){
                    return false;
                }
                if(typeof ds === 'string' && ds.length >= 8){
                    if(ds.substr(0, 10) === v){
                        return true;
                    }
                }
            }
            return false;
        };

        var isOrgnr = function(f){
            return parseInt(f.substr(2, 2)) >= 20;
        };

        var isSamordningsnr = function(f, century){
            if(parseInt(f.substr(4, 2)) < 61)return false;

            return realDate(century + f.substr(0, 2) + '-' + f.substr(2, 2) + '-' + (parseInt(f.substr(4, 2) - 60)));
        };

        if(!checkedFormat.valid)return {valid: false, error: 'INVALID_FORMAT'};
        else if(checkedFormat.type == 'o'){
            if(!isOrgnr(checkedFormat.standardFormat)){
                if(checkLuhn(checkedFormat.standardFormat)){
                    return {valid: false, error: '10DIGIT_PER'};
                }else{
                    return {valid: false, error: 'ORG_LUHN'};
                }
            }
            // Orgnr
            if(!checkLuhn(checkedFormat.standardFormat)){
                return {valid: false, error: 'ORG_LUHN'};
            }
            return {valid: true, type: 'ORG', formatted: checkedFormat.appFormat};
        }else{
            if(isOrgnr(checkedFormat.standardFormat)){
                if(checkLuhn(checkedFormat.standardFormat)){
                    return {valid: false, error: '12DIGIT_ORG'};
                }else{
                    return {valid: false, error: 'PER_LUHN'};
                }
            }
            if(isSamordningsnr(checkedFormat.standardFormat, checkedFormat.century)){
                // Samordningsnr
                if(!checkLuhn(checkedFormat.standardFormat)){
                    return {valid: false, error: 'SAM_LUHN'};
                }
                return {valid: true, type: 'SAM', formatted: checkedFormat.appFormat};
            }
            // Personnr
            if(!realDate(checkedFormat.century
                        + checkedFormat.standardFormat.substr(0, 2) + '-'
                        + checkedFormat.standardFormat.substr(2, 2) + '-'
                        + checkedFormat.standardFormat.substr(4, 2))){
                return {valid: false, error: 'PER_DATE'};
            }
            if(!checkLuhn(checkedFormat.standardFormat)){
                return {valid: false, error: 'PER_LUHN'};
            }
            return {valid: true, type: 'PER', formatted: checkedFormat.appFormat};
        }
    };
}]);
