angular.module('app.directives')
.directive('realDate', function(){
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                var d = new Date(viewValue);
                var valid = false;
                if(typeof d === 'object'){
                    var ds = d.toISOString();
                    if(typeof ds === 'string' && ds.length >= 8){
                        if(ds.substr(0, 10) === viewValue){
                            ctrl.$setValidity('realDate', true);
                            return viewValue;
                        }
                    }
                }
                ctrl.$setValidity('realDate', false);
                return undefined;
            });
        }
    };
});
