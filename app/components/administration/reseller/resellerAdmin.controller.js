angular.module('app.controllers')
.controller('ResellerAdminCtrl', [ '$scope','$state', 'ResellerHandler', 'Feedback', 'WaitLock', function($scope, $state, ResellerHandler, Feedback, WaitLock){
    $scope.state = $state;
    
    var loadResellers = function() {
    	if (WaitLock.lock()) return;
    	Feedback.http(
    		'admin.resellers.get',
    		ResellerHandler.getResellers(),
    		{
        		'4XX': 'Kunde inte hämta återförsäljare. Kontrollera dina behörigheter.',
        		'5XX': 'Ett fel inträffade. Kunde inte hämta återförsäljare. Försök igen senare.'	
    	})
    	.then(function(res){
    		$scope.resellers = res.data;
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.resellers.get')
                	.add('admin.resellers.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		
        })
        ['finally'](function(){
        	WaitLock.unlock();
        });   				
    };
    loadResellers();
    
    $scope.removeReseller = function(resellerId){
        if(WaitLock.check())return;
        Feedback.http('admin.resellers.edit', ResellerHandler.deleteReseller(resellerId), {
            403: 'Ej behörig att radera uppgifter för återförsäljare',
            '4XX': 'Uppgifterna för återförsäljaren kunde inte raderas',
            '5XX': 'Ett fel inträffade. Uppgifterna för återförsäljaren kunde inte raderas. Försök igen senare.'
        })
        ['finally'](function(){
            loadResellers();
        });
    };

    $scope.editReseller = function(reseller){
    	$scope.reseller = reseller;
    	$state.go('.update');
    };
    
}]);
