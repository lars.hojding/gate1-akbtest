angular.module('app.controllers')
.controller('ResellerUpdateCtrl', [ '$scope','$state', 'ResellerHandler', 'Feedback', 'WaitLock', function($scope, $state, ResellerHandler, Feedback, WaitLock){

	$scope.origReseller = angular.copy($scope.reseller);

	$scope.undoEdit = function(form){
		if(WaitLock.check()) return;
		angular.copy($scope.origReseller, $scope.reseller);
		$state.go('^');
	};
	
	$scope.saveReseller = function(form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('user.administration.reseller.update').add('user.administration.reseller.update',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('user.administration.reseller.update').add('user.administration.reseller.update',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('user.administration.reseller.update',
        		ResellerHandler.updateReseller($scope.reseller),
        	{
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att uppdatera återförsäljare',
            409: 'Återförsäljare finns redan',
            '4XX': 'Kunde inte uppdatera återförsäljare',
            '5XX': 'Ett fel inträffade. Kunde inte uppdatera återförsäljare. Försök igen senare.'
        })
        .then(function(){
            WaitLock.unlock();
            $state.go('^')
        })
        ['finally'](function(){
            WaitLock.unlock();
        });

	};
}]);
