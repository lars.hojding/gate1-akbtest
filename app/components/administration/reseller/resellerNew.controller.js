angular.module('app.controllers')
.controller('ResellerNewCtrl', [ '$scope','$state', 'ResellerHandler', 'Feedback', 'WaitLock', function($scope, $state, ResellerHandler, Feedback, WaitLock){
	$scope.reseller = {
		"resellerId": "",
		"name": "",
		"price": 0,
        "address": {
            "cOAddress": "",
            "streetAddress": "",
            "zipCode": "",
            "city": "",
            "country": ""
        },
        "contactDetails": {
        	"phoneNumber": "",
        	"email": ""
        },
	};
	
	$scope.saveReseller = function(form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('user.administration.reseller.create').add('user.administration.reseller.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('user.administration.reseller.create').add('user.administration.reseller.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('user.administration.reseller.create',
        		ResellerHandler.createReseller($scope.reseller),
        	{
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att skapa återförsäljare',
            409: 'Återförsäljare finns redan',
            '4XX': 'Kunde inte skapa återförsäljare',
            '5XX': 'Ett fel inträffade. Kunde inte skapa återförsäljare. Försök igen senare.'
        })
        .then(function(){
            WaitLock.unlock();
            $state.go('^', {}, {reload: true});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });

	};
}]);
