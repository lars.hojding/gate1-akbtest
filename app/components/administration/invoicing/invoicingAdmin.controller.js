angular.module('app.controllers')
.controller('InvoicingAdminCtrl', [ '$scope','$state', 'SubscriptionHandler', 'InvoiceHandler', 'Feedback', 'WaitLock', function($scope, $state, SubscriptionHandler, InvoiceHandler, Feedback, WaitLock){
    $scope.state = $state;
    var getInvoiceTimers = function() {
    	Feedback.http(
    	  'admin.invoice.get',
    	  InvoiceHandler.getInvoiceTimers(),
    	  {
    		  '4XX': 'Kunde inte hämta timers. Kontrollera dina behörigheter.',
    		  '5XX': 'Ett fel inträffade. Kunde inte hämta timers. Försök igen senare.'
    	  })
    	  .then(function(res){
    		  $scope.timer = res.data[0];
    	  },
    	  function(err){
    		  if(err.errorCode == 'UNKNOWN_ERROR'){
    			  Feedback.clearTarget('admin.invoice.get')
    			  	.add('admin.invoice.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    		  }
    	  });
    };
    $scope.loadInvoiceStatus = function() {
    	Feedback.http(
    	  'admin.invoice.get',
    	  InvoiceHandler.getInvoiceStatus(),
    	{
        	'4XX': 'Kunde inte hämta faktureringsstatus. Kontrollera dina behörigheter.',
        	'5XX': 'Ett fel inträffade. Kunde inte hämta faktureringsstatus. Försök igen senare.'	
    	})
    	.then(function(res){
    		$scope.invoiceStatus = res.data;
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.invoice.get')
                	.add('admin.invoice.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		
        });   				
    };
    $scope.loadInvoiceStatus();
    getInvoiceTimers();

    $scope.startInvoiceBatch = function(){
        if(WaitLock.lock())return;
        Feedback.http(
        	'admin.invoice.batch',
        	InvoiceHandler.startInvoiceBatch(),
        	{
        		'4XX': 'Kunde inte starta fakturering',
        		'5XX': 'Ett fel inträffade. Kunde inte starta fakturering. Försök igen senare.'
        	})
        .then(function(res){
        	$scope.invoiceStatus.running = true;
        	Feedback.clearTarget('admin.invoice.batch')
        		.add('admin.invoice.batch', Feedback.types.SUCCESS, 200, 'Fakturering startad');
        },
        function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.invoice.batch')
                	.add('admin.invoice.batch', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}else{
        		Feedback.clearTarget('admin.invoice.batch').add('admin.invoice.batch', Feedback.types.FAILURE, 9999, "Fakturering har inte startats. Försök senare.");
            }
        })
        ['finally'](function(){
            WaitLock.unlock();
            $scope.loadInvoiceStatus();
        });
    };
    
    $scope.checkInvoiceForRenewal = function() {
    	if(WaitLock.lock()) return;
    	Feedback.http(
    		'admin.invoice.batch',
    		SubscriptionHandler.markForRenewal(),
    		{
    			'4XX': 'Kunde inte starta kontroll',
    			'5XX': 'Ett fel inträffade. Kunde starta kontroll. Försök igen senare.'
    		})
    	.then(function(res){
        	Feedback.clearTarget('admin.invoice.batch')
    		.add('admin.invoice.batch', Feedback.types.SUCCESS, 200, 'Kontroll startad');
    		
    	},
    	function(err){
    		Feedback.clearTarget('admin.invoice.batch')
    			.add('admin.invoice.batch', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    		$scope.loadInvoiceStatus();
    	});
    };
}]);
