angular.module('app.controllers')
.controller('ReviewCtrl', [ '$scope','$state', 'InvoiceHandler', 'Feedback', 'WaitLock', function($scope, $state, InvoiceHandler, Feedback, WaitLock){
    $scope.state = $state;

    $scope.terminate = function(subscription){
    	if(WaitLock.lock()) return;
    	Feedback.http(
    		'admin.invoice.renew',
    		InvoiceHandler.terminate(subscription.orgNr),
    		{
    			400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
    			403: 'Ej behörig att avsluta abonnemang',
    			'4XX': 'Abonnemang kunde inte avslutas',
    			'5XX': 'Ett fel inträffade. Abonnemang kunde inte avslutas. Försök igen senare.'
    		})
    	.then(function(res){
    	},
    	function(err){
    		if(err.errorCode == 'UNKNOWN_ERROR'){
    			Feedback.clearTarget('admin.invoice.renew')
    				.add('admin.invoice.renew', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    		}
    		else{
    			Feedback.clearTarget('admin.invoice.renew').add('admin.invoice.renew', Feedback.types.FAILURE, 9999, 'Kunde inte avsluta abonnemang. Försök senare.');
    		}
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    		$scope.loadInvoiceStatus();
    	})
    };
    
    $scope.renew = function(subscription){
    	if(WaitLock.lock()) return;
    	Feedback.http(
    		'admin.invoice.renew',
    		InvoiceHandler.renew(subscription.orgNr),
    		{
                400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
                403: 'Ej behörig att förnya abonnemang',
                '4XX': 'Abonnemang kunde inte förnyas',
                '5XX': 'Ett fel inträffade. Abonnemang kunde inte förnyas. Försök igen senare.'

    		})
    	.then(function(res){
    		if(res.status == 204){
    			$scope.batchRunning = false;
    			Feedback.clearTarget('admin.invoice.renew')
    			.add('admin.invoice.renew', Feedback.types.FAILURE, 204, 'Abonnemanget kan inte förnyas.');
    		}
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.invoice.renew')
                	.add('admin.invoice.renew', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}else{
        		Feedback.clearTarget('admin.invoice.renew').add('admin.invoice.renew', Feedback.types.FAILURE, 9999, "Kunde inte förnya abonnemang. Försök senare.");
            }    		
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    		$scope.loadInvoiceStatus();
    	})
    };
    
}]);
