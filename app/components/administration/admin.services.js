angular.module('app.services')
.factory('ResellerHandler', ['$rootScope', 'AuthHttp', function($rootScope, AuthHttp){
    var baseURL = $rootScope.restUrl;
    return {
        getReseller: function(resellerId){
            return AuthHttp.get('reseller/', {params: {resellerId: resellerId}});
        },
        getResellers: function(){
        	return AuthHttp.get('reseller/all');
        },
        updateReseller: function(resellerData){
        	return AuthHttp.put('reseller/', resellerData);
        },
        createReseller: function(resellerData){
        	return AuthHttp.post('reseller/', resellerData);
        },
        deleteReseller: function(resellerId){
        	return AuthHttp['delete']('reseller/', {params: {resellerId: resellerId}});
        }
    };
}])

.factory('InvoiceHandler', ['$rootScope', 'AuthHttp', function($rootScope, AuthHttp){
	var baseUrl = $rootScope.restUrl;
	return {
		getInvoiceStatus: function(){
			return AuthHttp.get('invoice/status');
		},
		startInvoiceBatch: function(){
			return AuthHttp.postQ('invoice');
		},
		getInvoiceTimers: function(){
			return AuthHttp.get('invoice/timers');
		},
		renew: function(orgNr){
			return AuthHttp.postQ('subscription/renew', {orgNr: orgNr})
		},
		terminate: function(orgNr){
			return AuthHttp.postQ('subscription/terminate', {orgNr: orgNr})
		}
	};                        
}])

.factory('SubscriptionHandler', ['$rootScope', 'AuthHttp', function($rootScope, AuthHttp){
	var baseUrl = $rootScope.restUrl;
	return {
		deleteSubscription: function(orgNr){
			return AuthHttp.get('batch/deleteSubscription', {params: {orgNr: orgNr}});
		},
	    markForRenewal: function(){
	    	return AuthHttp.put('subscription/markRenewal');
	    }
	};
}]);
