angular.module('app.controllers')
.controller('SubscriptionAdminCtrl', [ '$scope','$state', 'currUser', 'SubscriptionHandler', 'Feedback', 'WaitLock', function($scope, $state, currUser, SubscriptionHandler, Feedback, WaitLock){
    $scope.state = $state;
    $scope.orgNr = $state.params.orgNr;
    $scope.batchRunning = false;
    if(typeof currUser == 'object'){
    	$scope.isSystemUser = currUser.isSystemUser;
    }
    
    $scope.deleteSubscription = function(orgNr){
        if(WaitLock.check())return;
        Feedback.http(
            	'admin.subscription.delete',
            	SubscriptionHandler.deleteSubscription(orgNr),
            	{
            		'4XX': 'Kunde inte ta bort bolag',
            		'5XX': 'Ett fel inträffade. Kunde inte ta bort bolag. Försök igen senare.'
            	})
            .then(function(res){
        		if(res.status == 204){
        			$scope.batchRunning = false;
        			Feedback.clearTarget('admin.subscription.delete')
        			.add('admin.subscription.delete', Feedback.types.FAILURE, 204, 'Bolaget finns inte');
        		}
        		else{
        			$scope.batchRunning = true;
        			Feedback.clearTarget('admin.subscription.delete')
        			.add('admin.subscription.delete', Feedback.types.SUCCESS, 200, 'Borttag startat');
        		}
            },
            function(err){
            	$scope.batchRunning = false;
            	if(err.errorCode === 'UNKNOWN_ERROR'){
            		Feedback.clearTarget('admin.subscription.delete')
                    	.add('admin.subscription.delete', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
            	}else{
            		Feedback.clearTarget('admin.subscription.delete').add('admin.subscription.delete', Feedback.types.FAILURE, 9999, "Borttag har inte startats. Försök senare.");
                }
            })
            ['finally'](function(){
                WaitLock.unlock();
            });
    };

    
}]);
