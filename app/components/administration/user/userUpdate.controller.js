angular.module('app.controllers')
.controller('UserUpdateCtrl', [ '$scope','$state', 'UserHandler', 'ResellerHandler', 'Feedback', 'WaitLock', function($scope, $state, UserHandler, ResellerHandler, Feedback, WaitLock){

	$scope.origUser = angular.copy($scope.selectedUser.user);

	var loadResellers = function() {
    	if (WaitLock.lock()) return;
    	Feedback.http(
    		'admin.resellers.get',
    		ResellerHandler.getResellers(),
    		{
        		'4XX': 'Kunde inte hämta återförsäljare. Kontrollera dina behörigheter.',
        		'5XX': 'Ett fel inträffade. Kunde inte hämta återförsäljare. Försök igen senare.'	
    	})
    	.then(function(res){
    		$scope.resellers = res.data;
    		var empty = {name: "Ingen återförsäljare"};
    		$scope.resellers.splice(0,0,empty);
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.resellers.get')
                	.add('admin.resellers.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		
        })
        ['finally'](function(){
        	WaitLock.unlock();
        });   				
    };
    loadResellers();

	$scope.undoEdit = function(form){
		if(WaitLock.check()) return;
		angular.copy($scope.origUser, $scope.selectedUser.user);
		$state.go('^');
	};
	
	$scope.saveUser = function(form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('user.administration.user.update').add('user.administration.user.update',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('user.administration.user.update').add('user.administration.user.update',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('user.administration.user.update',
        		UserHandler.updateUser($scope.selectedUser.user),
        	{
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att uppdatera användare',
            409: 'Användare finns redan',
            '4XX': 'Kunde inte uppdatera användare',
            '5XX': 'Ett fel inträffade. Kunde inte uppdatera användare. Försök igen senare.'
        })
        .then(function(res){
        	return UserHandler.getUser(res.data.userId);
        })
        .then(function(res){
        	$scope.selectedUser.user.resellerId = res.data.resellerId;
        	$scope.selectedUser.user.resellerName = res.data.resellerName;
        	$scope.selectedUser.user.price = res.data.price;
            WaitLock.unlock();
            $state.go('^')
        })
        ['finally'](function(){
            WaitLock.unlock();
        });

	};
}]);
