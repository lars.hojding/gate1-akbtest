angular.module('app.controllers')
.controller('UserAdminCtrl', [ '$scope','$state', 'UserHandler', 'Feedback', 'WaitLock', function($scope, $state, UserHandler, Feedback, WaitLock){
    $scope.state = $state;
    
    var usersPerPage = 10;

    $scope.goToPage = function(newPageNumber, oldPageNumber){
    	var offset = (newPageNumber -1) * usersPerPage;
    	if(WaitLock.lock()) return;
    	Feedback.http(
    	'admin.users.get',
    	UserHandler.getUsers(offset, usersPerPage),
    	{
    		'4XX': 'Kunde inte hämta användare. Kontrollera dina behörigheter.',
    		'5XX': 'Ett fel inträffade. Kunde inte hämta användare. Försök igen senare.'
    	})
    	.then(function(res){
    		$scope.users = res.data.resultlist;
    		$scope.offset = res.data.offset;
    		$scope.limit = res.data.limit;
    		$scope.total = res.data.total;
    	},
    	function(err){
    		if(err.errorCode == 'UNKNOWN_ERROR'){
    			Feedback.clearTarget('admin.users.get')
    			.add('admin.users.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    		}
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    	});
    };
    
    var loadUsers = function() {
    	$scope.goToPage(1,1);
    };
    loadUsers();
    
    
    $scope.activate = function(user){
    	user.userStatus = 1;
    	updateUser(user);
    };
    
    $scope.inactivate = function(user){
    	user.userStatus = 0;
    	updateUser(user);
    };
    
    var updateUser = function(user) {
    	if (WaitLock.lock()) return;
    	Feedback.http(
    		'admin.users.edit',
    		UserHandler.updateUser(user),
    		{
    			'403': 'Ej behörig att uppdatera användare',
        		'4XX': 'Kunde inte uppdatera användare.',
        		'5XX': 'Ett fel inträffade. Kunde inte uppdatera användare. Försök igen senare.'	
    	})
    	.then(function(res){
    		user = res.data;
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('admin.users.edit')
                	.add('admin.users.edit', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    	});
    };
    
    $scope.editUser = function(user){
    	$scope.selectedUser = {};
    	$scope.selectedUser.user = user;
    	$state.go('.update');
    };
}]);
