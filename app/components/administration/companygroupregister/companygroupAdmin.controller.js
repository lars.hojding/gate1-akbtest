angular.module('app.controllers')
.controller('CompanygroupAdminCtrl', [ '$scope','$state', 'Holding', 'Feedback', 'WaitLock', function($scope, $state, Holding, Feedback, WaitLock){
    $scope.state = $state;
    $scope.form = {};
    
    $scope.fetchRecords = function(orgNr){
    	if (!orgNr)
    		orgNr = $scope.form.fetchOrgNr;
    	Feedback.http(
    		'admin.companygroup.get',
    	    Holding.companygroup.getRecords(orgNr),
    	    {
    	    	'4XX': 'Kunde inte hämta koncern. Kontrollera dina behörigheter.',
    	    	'5XX': 'Ett fel inträffade. Kunde inte hämta koncern. Försök igen senare.'	
    	    })
    	    .then(function(res){
    	    	if(res.status == 204 || res.data.length < 1){
    	    		Feedback.clearTarget('admin.companygroup.get')
    	    		.add('admin.companygroup.get', Feedback.types.FAILURE, 205, 'Bolaget finns inte i Koncernregistret.');    			
    	    	}
    	    	else{
    	    		$scope.records = res.data;
    	    		$scope.companyName = res.data[0].companyName;
    	    		$scope.orgNr = res.data[0].orgNr;
    	    		$scope.fetched = true;
    	    	}
    	    },
    	    function(err){
    	    	$scope.fetched = false;
    	    	if(err.errorCode === 'UNKNOWN_ERROR'){
    	    		Feedback.clearTarget('admin.companygroup.get')
    	            .add('admin.companygroup.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    	        }else{
    	            Feedback.clearTarget('admin.companygroup.get').add('admin.companygroup.get', Feedback.types.FAILURE, 9999, "Kunde inte hämta koncern. Försök senare.");
    	        }    		
    	    })
    	};
    
    $scope.editRecord = function(record){
    	$scope.groupdataRecord = record;
    	$state.go('.update');
    };
    
}]);
