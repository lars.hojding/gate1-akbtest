angular.module('app.controllers')
.controller('UserSettingsCtrl', [ '$scope','$state', 'UserHandler', function($scope, $state, UserHandler){
    $scope.state = $state;
    UserHandler.getSelf().then(function(res){
    	if(typeof res.data == 'object'){
        	$scope.currUser = res.data;
    	}
    }, function(err){
    });
}]);
