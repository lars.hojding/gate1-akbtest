angular.module('app.services')
.factory('UserHandler', ['$rootScope', '$q', 'AuthHttp', 'Authentication', function($rootScope, $q, AuthHttp, Authentication){
    var baseURL = $rootScope.restUrl;
    return {
        getUser: function(userId){
            var deferred = $q.defer();
            Authentication.getAuthData()
            .then(function(resAuth){
                var authData = resAuth.data;
                var params = {userId: userId === null ? authData.uid : userId};
                AuthHttp.get('user/', {params: params})
                .then(function(resUser){
                    deferred.resolve({statusCode: 200, data: resUser.data});
                }, function(error){
                    deferred.reject({statusCode: error.status, data: error});
                });
            }, function(error){
                deferred.reject({statusCode: error.status, data: error});
            });
            return deferred.promise;
        },
        getSelf: function(){
            return this.getUser(null);
        },
        getUsers: function(offset, limit){
        	return AuthHttp.get('user/users', {params:{offset: offset, limit: limit}});
        },
        updateUser: function(data){
        	return AuthHttp.put('user', data);
        },
        acceptTerms: function(userId) {
        	return AuthHttp.get('user/acceptTerms', {params:{userId: userId}});
        }
    };
}]);

