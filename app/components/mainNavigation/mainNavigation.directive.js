angular.module('app.directives')
.directive('mainNavigation', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/mainNavigation/mainNavigation.template.html',
        controller: 'MainNavCtrl'
    }
    //MainNAVIGATION
})