angular.module('app.controllers')
.controller('MainNavCtrl', ['$scope','$state','$cookieStore', 'AuthHttp', function($scope, $state, $cookieStore, AuthHttp){

    $scope.$state = $state;

    $scope.navList = [
        { state: 'user.dashboard', title: 'Hem', class:'link', icon:'home_neg_trans.png'},
        { state: 'user.help.main', title: 'Hjälp', class:'link', icon:'help_neg_trans.png'}
    ];

    $scope.signout = function() {
        AuthHttp.logout();
    };


        $scope.showmenu=false;
        $scope.toggleMenu = function(){
            $scope.showmenu=($scope.showmenu) ? false : true;
        }
}]);
