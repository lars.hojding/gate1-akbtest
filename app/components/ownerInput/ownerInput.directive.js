angular.module('app.directives')
.directive('svabOwner', ['Owner', '$timeout', '$document', function(Owner, $timeout, $document){
    return {
        require: '^ngModel',
        restrict: 'E',
        scope: {
            orgNr: '@svabOrgnr',
            ownerId: '=ngModel',
            foreignOwner: '=svabForeignOwner',
            onSelected: '&svabSelected',
            onForeign: '&svabForeign',
            form: '=svabForm'
        },
        templateUrl: 'components/ownerInput/ownerInput.html',
        link: function(scope, elm, attrs, ngModel){
            scope.showDropdown = false;
            scope.owners = [];
            scope.obj = {ownerId : scope.ownerId};

            Owner.getPotentialOwners(scope.orgNr).then(function(res){
                scope.owners = res.data;
            });

            scope.doOnSelected = function(){
            	if(!scope.ownerId){
            		scope.ownerId = scope.obj.ownerId;
            	}
            	scope.onSelected();
            };

            scope.doOnForeignOwner = function(){
            	scope.onForeign();
            };
            
            scope.backdropClick = function(){
                scope.$apply(function(){
                    $document.off('click', scope.backdropClick);
                    scope.showDropdown = false;
                });
            };

            scope.toggleDropdown = function(){
                if(!scope.showDropdown){
                    scope.showDropdown = true;
                    $timeout(function(){
                        $document.on('click', scope.backdropClick);
                    });
                }
            };

            scope.selectDropdownItem = function(item){
	            scope.ownerId = item.ownerId;
	            scope.obj.ownerId = item.ownerId;
            	if(!item.foreign){
			        $timeout(function(){
			        	scope.form.ownerId.$setDirty();
			            scope.form.ownerId.$setTouched();
			            scope.doOnSelected();
			        });
            	}
            	else{
            		scope.foreignOwner = item;
            		$timeout(function(){
            			scope.doOnForeignOwner();
            		});
            	}
            };

            //$document.on('click', scope.backdropClick);
            scope.$on('$destroy', function(){
                $document.off('click', scope.backdropClick);
            });
        }
    };
}]);
