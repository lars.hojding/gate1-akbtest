angular.module('app.services')
.factory('Authentication', ['$rootScope', '$cookieStore', '$http', '$q', '$state', 'WaitLock',
            function($rootScope, $cookieStore, $http, $q, $state, WaitLock){
    var isExistingCookie = function(key){
        if(typeof $cookieStore === 'undefined')return false;
        if(typeof $cookieStore.get(key) === 'undefined')return false;
        if($cookieStore.get(key) === null)return false;
        if(('' + $cookieStore.get(key)).length == 0)return false;
        return true;
    };
    return {
        login: function(personalNumber){
            var deferred = $q.defer();
            if(typeof personalNumber === 'undefined' || personalNumber === null){
                deferred.reject({statusCode: 9999, status: 9999, data: null}); // No username
            }else{
                this.getAPIURL().then(function(baseURL){
                    $http.get(baseURL + 'authenticate/testlogin', {params: {personalNumber: personalNumber}})
                    .then(function(res){
                        if(res.data.progressStatus === 'COMPLETE'){
                            $cookieStore.put('uid', res.data.authenticatedUser.userId);
                            $cookieStore.put('token', res.data.token);
                            deferred.resolve({statusCode: 200, status: 200, data: null});
                        }else{
                            deferred.reject({status: 400});
                        }
                    }, function(err){
                        deferred.reject({status: 400});
                    });
                }, function(){
                    deferred.reject({status: 500});
                });
            }
            return deferred.promise;
        },
        bankIdStart: function(personalNumber){
            $cookieStore.remove('uid');
            $cookieStore.remove('token');
            if(typeof personalNumber === 'undefined' || personalNumber === null){
                // BankID
                var p = this.getAPIURL().then(function(baseURL){
                    return $http.get(baseURL + 'authenticate/startlogin', {params: {
                    	orgnr: $state.params.orgnr,
                    	email: $state.params.email,
                    	phone: $state.params.phone,
                    	firstName: $state.params.firstName,
                    	lastName: $state.params.lastName,
                    	clientType: $state.params.clientType}
                    });
                }, function(){
                    $q.reject({status: 500});
                });
            }else{
                // Mobilt BankID
                var p = this.getAPIURL().then(function(baseURL){
                    return $http.get(baseURL + 'authenticate/startlogin', {params: {
                        personalNumber: personalNumber,
                    	orgnr: $state.params.orgnr,
                    	email: $state.params.email,
                    	phone: $state.params.phone,
                    	firstName: $state.params.firstName,
                    	lastName: $state.params.lastName,
                    	clientType: $state.params.clientType}
                    });
                }, function(){
                    $q.reject({status: 500});
                });
            }
            return p.then(function(res){
                if(res.errorCode === 'ALREADY_IN_PROGRESS'){
                    $q.reject(res);
                }
                return res;
            });
        },
        bankIdFinish: function(sessionId){
        	var deferred = $q.defer();
        	this.getAPIURL().then(function(baseURL){
        		$http.get(baseURL + 'authenticate/finishlogin', {params:{sessionId: sessionId}})
        		.then(function(collectRes){
        			if(collectRes.data.progressStatus == 'COMPLETE') {
        				$cookieStore.put('uid', collectRes.data.authenticatedUser.userId);
        				$cookieStore.put('token', collectRes.data.token);
        				deferred.resolve(collectRes.data);
        			} else if (typeof collectRes.data.progressStatus == 'undefined'){
        				deferred.reject(collectRes.data);
        			}
        		}, function(err){
        			deferred.reject(err);
        		});
        	}, function(){
        		deferred.reject({status: 500});
        	});
        	return deferred.promise;
        },
        cancelLogin: function(){
            bankidPollingRunning = false;
        },
        logout: function(){
            $cookieStore.remove('uid');
            $cookieStore.remove('token');
            $state.go('anon');        	
        },
        getAuthData: function(){
            var deferred = $q.defer();
            if(isExistingCookie('uid') === true && isExistingCookie('token') === true){
                deferred.resolve({statusCode: 200, status: 200, data: {
                    uid: $cookieStore.get('uid'),
                    token: $cookieStore.get('token')
                }});
            }else{
                deferred.reject({statusCode: 401, status: 401, data: null});
                this.logout();
            }
            return deferred.promise;
        },
        getAuthDataBlocking: function(){
            if(isExistingCookie('uid') === true && isExistingCookie('token') === true){
                return {
                    statusCode: 200, status: 200, data: {
                        uid: $cookieStore.get('uid'),
                        token: $cookieStore.get('token')
                    }
                };
            }
            return {statusCode: 401, status: 401, data: null};
        },
        getAPIURL: function(){
            var deferred = $q.defer();

            if($rootScope.restUrl == ''){
                $http.get('apiurl.txt')
                .then(function(result){
                    var ret = function(){
                        deferred.resolve($rootScope.restUrl);
                    };
                    $rootScope.restUrl = result.data;
                    $http.get($rootScope.restUrl + 'healthcheck/ping').then(ret, ret);
                }, function(){
                    deferred.reject('');
                });
            }else{
                deferred.resolve($rootScope.restUrl);
            }

            return deferred.promise;
        },
        tryAPIConnection: function(){
            this.getAPIURL().then(function(baseURL){
                $http.get(baseURL + '');
            });
        },
        isTestEnv: function(){
            return $http.get('env-type.txt')
            .then(function(res){
                if(res.data == 'test'){
                    return {test: true};
                }else{
                    return $q.reject({test: false});
                }
            }, function(){
                return $q.reject({test: false});
            });
        }
    };
}])

.factory('AuthHttp', ['$http', '$q',  'Authentication', '$cookieStore', '$state', function($http, $q, Authentication, $cookieStore, $state){
    var hasParams = function(c, v){
        if(typeof v === 'undefined')v = 'params';
        if(typeof c === 'undefined')return false;
        if(c === null)return false;
        if(typeof c[v] === 'undefined')return false;
        if(c[v] === null)return false;
        return true;
    };
    var buildConfig = function(config, method){
        return Authentication.getAuthData()
        .then(function(res){
            var authData = res.data;
            var c = hasParams(config) ? config : {params: {}};
            c.params.token = res.data.token;
            return c;
        });
    };
    var handleNotLoggedIn = function(promise){
        return promise
        .catch(function(err){
            if(err.statusCode === 401 || err.status === 401){
                Authentication.logout();
            }
            return $q.reject(err);
        });
    };
    var handleErrorStatus = function(promise){
        return promise.then(function(res){
            if(res.status >= 300){
                return $q.reject(res);
            }
            // Handle special cases here
            return promise;
        });
    };
    return {
        get: function(action, config){
            return Authentication.getAPIURL().then(function(baseURL){
                return buildConfig(config, action)
                .then(function(c){
                    return handleNotLoggedIn(
                        handleErrorStatus($http.get(baseURL + action, c)));
                });
            }, function(){
                $q.reject({status: 401});
            });
        },
        post: function(action, data, config){
            return Authentication.getAPIURL().then(function(baseURL){
                return buildConfig(config, action)
                .then(function(c){
                    if(typeof data === 'object' && data !== null && typeof c.params.token !== 'undefined' && c.params.token !== null){
                        data.token = c.params.token;
                    }
                    return handleNotLoggedIn(
                        handleErrorStatus($http.post(baseURL + action, data, c)));
                });
            }, function(){
                $q.reject({status: 401});
            });
        },
        postQ: function(action, data, config){
            return Authentication.getAPIURL().then(function(baseURL){
                return buildConfig(config, action)
                .then(function(c){
                    if(typeof data === 'object' && data !== null && typeof c.params.token !== 'undefined' && c.params.token !== null){
                        data.token = c.params.token;
                    }
                    var q = '';
                    angular.forEach(data, function(v, k){
                        if(k != 'token'){
                            if(q.length == 0){
                                q = '?' + k + '=' + v;
                            }else{
                                q += '&' + k + '=' + v;
                            }
                        }
                    });

                    return handleNotLoggedIn(
                        handleErrorStatus($http.post(baseURL + action + q, {}, c)));
                });
            }, function(){
                $q.reject({status: 401});
            });
        },
        put: function(action, data, config){
            return Authentication.getAPIURL().then(function(baseURL){
                return buildConfig(config, action)
                .then(function(c){
                    if(typeof data === 'object' && data !== null && typeof c.params.token !== 'undefined' && c.params.token !== null){
                        data.token = c.params.token;
                    }
                    return handleNotLoggedIn(
                        handleErrorStatus($http.put(baseURL + action, data, c)));
                });
                return deferred.promise;
            }, function(){
                $q.reject({status: 401});
            });
        },
        "delete": function(action, config){
            return Authentication.getAPIURL().then(function(baseURL){
                return buildConfig(config, action)
                .then(function(c){
                    return handleNotLoggedIn(
                        handleErrorStatus($http["delete"](baseURL + action, c)));
                });
            }, function(){
                $q.reject({status: 401});
            });
        },
        logout: function(){
        	return this.get('authenticate/logout').then(function(){
                $cookieStore.remove('uid');
                $cookieStore.remove('token');
                $state.go('anon');
        	});
        }
    };
}]);
