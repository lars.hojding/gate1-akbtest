angular.module('app.services')
.factory('Report', ['$rootScope', '$q', '$interval', 'AuthHttp', 'Authentication', 'WaitLock', function($rootScope, $q, $interval, AuthHttp, Authentication, WaitLock){
	var baseUrl = $rootScope.restUrl;
	var reportPollingTimer = null;
	var reportPollingRunning = false;
    return {
        orderReport: function(orgNr, documentType, fromDate, toDate){
            var params = {
                target: orgNr,
                documentType: documentType
            };
            if(typeof fromDate !== 'undefined' && fromDate !== null){
                params.fromDate = fromDate;
            }
            if(typeof toDate !== 'undefined' && toDate !== null){
                params.toDate = toDate;
            }
            return AuthHttp.postQ('report/order', params);
        },    
    	reportStatus: function(orgNr, offset, limit){
    		return AuthHttp.get('report/reports/', {params: {orgNr: orgNr, offset: offset, limit: limit}});
    	},
    	checkReport: function(docnumber){
    		return AuthHttp.get('report/status/', {params: {documentNumber: docnumber}});
    	},
    	reportUrl: function(orgNr, fileName){
    		var authRes = Authentication.getAuthDataBlocking()
    		if(200 == authRes.status){
    			return baseUrl + 'report/fetch/' + orgNr + '/' + fileName + '?token=' + authRes.data.token;
    		}
    		else {
    			return '';
    		}    				
    	},
    	invalidateReport: function(orgNr, docnumber){
    		return AuthHttp.get('report/invalidate', {params: {orgNr: orgNr, documentNumber: docnumber}});
    	},
    	waitForReport: function(documentNumber){
    		var deferred = $q.defer();
    		
    		var numTriesLeft = 15;
    		var tryReport = function(){
                if(WaitLock.lock('reportPolling'))return;
                if(!reportPollingRunning){
                    $interval.cancel(reportPollingTimer);
                    deferred.reject({errorCode: 'USER_CANCEL'});
                    WaitLock.unlock('reportPolling');
                    return;
                }
                if(numTriesLeft-- <= 0){
                    deferred.reject({status: 403, data: {errorCode: 'Åtgärden tog för lång tid. Försök igen.'}});
                    WaitLock.unlock('reportPolling');
                    return;
                }
                AuthHttp.get('report/status', {params:{documentNumber: documentNumber}})
                .then(function(res){
                    if(res.data.status == 4){
                        $interval.cancel(reportPollingTimer);
                        deferred.resolve(res);
                    }else{
                        deferred.notify(res.data);
                    }
                    WaitLock.unlock('reportPolling');
                }, function(err){
                    $interval.cancel(reportPollingTimer);
                    deferred.reject(err);
                });
    		};

	        reportPollingRunning = true;
	        if(reportPollingTimer !== null){
	            $interval.cancel(reportPollingTimer);
	        };
	        reportPollingTimer = $interval(tryReport, 1800);
	        $rootScope.$on('$destroy', function(){
	            $interval.cancel(reportPollingTimer);
	        });
	        $rootScope.$on('$stateChangeStart', function(){
	            $interval.cancel(reportPollingTimer);
	        });
	        return deferred.promise;    			
    	}
    };
}]);
