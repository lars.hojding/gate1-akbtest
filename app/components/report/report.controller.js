angular.module('app.controllers')
.controller('ReportCtrl', ['$scope', '$window', 'Report', '$stateParams', 'Feedback', 'WaitLock', 'holdings',
                function ($scope, $window, Report, $stateParams, Feedback, WaitLock, holdings){
    
	var orgNr = $stateParams.orgNr;
	var reportsPerPage = 5;
	$scope.hasHoldings = typeof holdings == 'object' && holdings.length > 0;
    $scope.docstatuses = [];
    $scope.report = {};
    $scope.fetchingReport = false;
    if($scope.companyType == 'AB'){
    	$scope.report.documentType = '2';
    }
    if($scope.companyType == 'HB'){
    	$scope.report.documentType = '6';
    }
    
    $scope.orderReport = function(reportFormData){
        if(WaitLock.lock())return;
        $scope.fetchingReport = true;
        Feedback.http(
        	'report.order',
        	Report.orderReport(orgNr, reportFormData.documentType, reportFormData.fromDate, reportFormData.toDate),
        	{
        		'4XX': 'Kunde inte beställa rapport',
        		'5XX': 'Ett fel inträffade. Kunde inte beställa rapport. Försök igen senare.'
        	})
        	.then(function(res){
        		return Report.waitForReport(res.data.documentNumber);
        	}
        )
        .then(function(res){
        		$scope.openReport(res.data.fileName);
        		loadReports();
        	},
        function(err){
        		if(err.errorCode === 'UNKNOWN_ERROR'){
                    Feedback.clearTarget('report.order')
                    .add('report.order', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
                }else{
                    Feedback.clearTarget('report.order').add('report.order', Feedback.types.FAILURE, 9999, "Rapporten har inte skapats. Försök senare.");
                }
            }, 
        function(notifyValue){
            }
        )
        ['finally'](function(){
        	$scope.fetchingReport = false;
            WaitLock.unlock();
        });
    };
    
    $scope.openReport = function(fileName){
    	var reportUrl = Report.reportUrl(orgNr, fileName);
    	$window.open(reportUrl);
    };
    
    $scope.goToPage = function(newPageNumber, oldPageNumber){
    	var offset = reportsPerPage * (newPageNumber -1);
    	Feedback.http('report.status.all',
    		Report.reportStatus(orgNr, offset, reportsPerPage),
    		{
    			'4XX': 'Kunde inte hämta rapporter',
    			'5XX': 'Ett fel inträffade. Kunde inte hämta rapporter. Försök igen senare.'
    		})
    		.then(function(res){
    		$scope.currentPage = newPageNumber;
    		$scope.docstatuses = res.data.resultlist;
    		$scope.offset = res.data.offset;
    		$scope.limit = res.data.limit;
    		$scope.total = res.data.total;
    	});
    };
    
    $scope.invalidateReport = function(documentNumber){
    	Feedback.http('report.status.all',
    		Report.invalidateReport(orgNr, documentNumber),
    		{
    			'4XX': 'Kunde inte ta bort rapport',
    			'5XX': 'Ett fel inträffade. Kunde inte ta bort rapport. Försök igen senare.'
    		})
    		.then(function(res){
    		$scope.goToPage($scope.currentPage,1)
    	});
    };
    
    var loadReports = function(){
    	$scope.goToPage(1,1);
    };
    loadReports();
}]);
