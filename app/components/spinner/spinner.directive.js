angular.module('app.directives')
.directive('svabSpinner', [function(){
    return {
        template: '<span class="svab-spinner-outer"><span class="svab-spinner-circle"></span></span>'
    };
}]);