angular.module('app.controllers')
.controller('OwnersCtrl', ['$scope', 'Owner', '$stateParams', 'Feedback',
                function ($scope, Owner, $stateParams, Feedback){
    var orgNr = $stateParams.orgNr;

    $scope.owners = [];

    var loadOwners = function(){
        Feedback.http('owner.list.get', Owner.getOwners(orgNr), {
            403: 'Ej behörig att se ägaruppgifter',
            '4XX': 'Ägaruppgifter kunde inte hämtas',
            '5XX': 'Ett fel inträffade. Ägaruppgifter kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.owners = res.data;
        });
    };
    loadOwners();

}]);
