angular.module('app.controllers')
.controller('NewOwnerCtrl', ['$scope', 'Owner', '$stateParams', '$state', 'Feedback', 'companyType', 'WaitLock',
            function ($scope, Owner, $stateParams, $state, Feedback, companyType, WaitLock){
    var orgNr = $stateParams.orgNr;

    $scope.getInputOwnerId = function(){
        if(typeof $stateParams.ownerId === 'undefined')return '';
        if(isNaN(parseInt($stateParams.ownerId)))return '';
        var a = parseInt($stateParams.ownerId);
        if(a > 0)return a;
        return '';
    };

    $scope.ownerIdType = function(ownerId){
        ownerId += '';
        if(ownerId.length != 10)return 1;
        var mm = ownerId.substr(2, 2);
        return parseInt(mm) < 20 ? 1 : 2;
    };

    $scope.fromTransaction = $state.current.name == 'user.company.owner.newFromTransaction';
    $scope.fromHolding = $state.current.name == 'user.company.owner.newFromHolding';
    $scope.foreignFromTransaction = $state.current.name == 'user.company.owner.newForeignOwnerFromTransaction';
    $scope.foreignFromHolding = $state.current.name == 'user.company.owner.newForeignOwnerFromHolding';

    $scope.createMode = true;
    $scope.owner = {
        "personOrCompany": $scope.ownerIdType($scope.getInputOwnerId()),
        "ownerId": $scope.getInputOwnerId(),
        "firstName": "",
        "lastName": "",
        "primaryAddress": {
            "cOAddress": "",
            "streetAddress": "",
            "zipCode": "",
            "city": "",
            "country": ""
        },
        "alternateAddress": {
            "cOAddress": "",
            "streetAddress": "",
            "zipCode": "",
            "city": "",
            "country": ""
        },
        "contactDetails": {
            "phoneNumber": "",
            "email": ""
        }
    };

    $scope.save = function(owner, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('owner.create').add('owner.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll alla obligatoriska fält');
            }else{
                Feedback.clearTarget('owner.create').add('owner.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('owner.create', Owner.createOwner($scope.owner), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till ägare',
            409: 'Ägaren redan tillagd',
            '4XX': 'Ägaruppgifter kunde inte läggas till',
            '5XX': 'Ett fel inträffade. Ägaruppgifter kunde inte läggas till. Försök igen senare.'
        })
        .then(function(res){
            if($scope.fromTransaction){
                // Go to transaction
                $state.go('user.company.transaction.newWithOwner', {orgNr: orgNr, ownerId: $scope.owner.ownerId});
            }else if($scope.fromHolding){
                // Go to holding
                $state.go('user.company.holding.newWithOwner', {orgNr: orgNr, ownerId: $scope.owner.ownerId});
            }else{
                // Go to owner list
                $state.go('user.company.owner.list', {orgNr: orgNr});
            }
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.saveForeign = function(owner, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('owner.create').add('owner.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll alla obligatoriska fält');
            }else{
                Feedback.clearTarget('owner.create').add('owner.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('owner.create', Owner.createForeignOwner($scope.owner), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till ägare',
            409: 'Ägaren redan tillagd',
            '4XX': 'Ägaruppgifter kunde inte läggas till',
            '5XX': 'Ett fel inträffade. Ägaruppgifter kunde inte läggas till. Försök igen senare.'
        })
        .then(function(res){
        	if($scope.foreignFromTransaction){
	            // Go to transaction
	            $state.go('user.company.transaction.newWithOwner', {orgNr: orgNr, ownerId: res.data.ownerId, foreignOwner: res.data});
        	}else if($scope.foreignFromHolding){
	            // Go to holding
	            $state.go('user.company.holding.newWithOwner', {orgNr: orgNr, ownerId: res.data.ownerId, foreignOwner: res.data});
            }else{
                // Go to owner list
                $state.go('user.company.owner.list', {orgNr: orgNr});
            }
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };
}]);
