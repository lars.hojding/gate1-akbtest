angular.module('app.services')
.factory('Owner', ['AuthHttp', '$q', function(AuthHttp, $q){
    return {
        getOwners: function(orgNr){
            return AuthHttp.get('owner/all', {params: {orgNr: orgNr}});
        },
        getPotentialOwners: function(orgNr){
        	return AuthHttp.get('owner/potential', {params: {orgNr: orgNr}});
        },
        createOwner: function(ownerData){
            return AuthHttp.post('owner', ownerData);
        },
        createForeignOwner: function(ownerData){
        	return AuthHttp.post('owner/foreign', ownerData);
        },
        getOwner: function(ownerId){
            return AuthHttp.get('owner', {params: {ownerId: ownerId}});
        },
        exists: function(ownerId){
            return this.getOwner(ownerId).then(function(res){
                if(res.status == 200){
                    return {exists: true, owner: res.data};
                }else{
                    return {exists: false};
                }
            });
        },
        existsInCompany: function(ownerId, orgNr){
            return AuthHttp.get('owner/exists', {params: {ownerId: ownerId, orgNr: orgNr}})
            .then(function(res){
                if(res.data.exists == false){
                    return $q.reject();
                }
            });
        }
    };
}]);
