angular.module('app.directives')
.directive('logotype', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/logotype/logotype.template.html'
    }

})