angular.module('app.directives')
.directive('activeUser', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/activeUser/activeUser.template.html',
        controller: 'activeUserCtrl as activeUser'
    }
    //MainNAVIGATION
});
