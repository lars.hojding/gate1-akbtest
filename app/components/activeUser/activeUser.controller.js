angular.module('app.controllers')
.controller('activeUserCtrl', ['$scope','UserHandler',  function ($scope, UserHandler){
	$scope.activeUser = false;
    UserHandler.getSelf().then(function(res){
        if(typeof res.data == 'object'){
        	$scope.currUser = res.data.firstName + ' ' + res.data.lastName;
        	$scope.activeUser = true;
        }
    }, function(err){
        $scope.currUser = '';
    });
}]);
