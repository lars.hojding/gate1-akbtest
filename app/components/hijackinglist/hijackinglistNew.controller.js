angular.module('app.controllers')
.controller('HijackinglistNewCtrl', ['$scope', 'Hijackinglist', '$stateParams', '$state', 'Feedback', 'WaitLock',
            function ($scope, Hijackinglist, $stateParams, $state, Feedback, WaitLock){
    $scope.entry = {
      "hijackingNoticeListId": 0,
      "firstName": "",
      "lastName": "",
      "contactDetails": {
        "phoneNumber": "",
        "email": ""
      },
      "orgNr": $stateParams.orgNr
    };

    $scope.save = function(hijackinglist, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('hijackinglist.create').add('hijackinglist.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll alla fält');
            }else{
                Feedback.clearTarget('hijackinglist.create').add('hijackinglist.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('hijackinglist.create', Hijackinglist.createEntry($scope.entry), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till i listan',
            409: 'Personen är redan tillagd',
            '4XX': 'Kunde inte lägga till i listan',
            '5XX': 'Ett fel inträffade. Kunde inte lägga till i listan. Försök igen senare.'
        })
        .then(function(){
            $state.go('user.company.hijackinglist.entries', {orgNr: $stateParams.orgNr});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };
}]);
