angular.module('app.services')
.factory('Hijackinglist', ['AuthHttp', function(AuthHttp){
    return {
        getEntries: function(orgNr){
            return AuthHttp.get('hijackinglist/', {params: {orgNr: orgNr}});
        },
        deleteEntry: function(entryData){
            return AuthHttp['delete']('hijackinglist/', {params: {
                orgNr: entryData.orgNr,
                hijackingNoticeListId: entryData.hijackingNoticeListId,
                hijackingListId: entryData.hijackingNoticeListId}});
        },
        updateEntry: function(entryData){
            return AuthHttp.put('hijackinglist/', entryData);
        },
        createEntry: function(entryData){
            return AuthHttp.post('hijackinglist/', entryData);
        }
    }
}]);
