angular.module('app.controllers')
.controller('HijackinglistCtrl', ['$scope', 'Hijackinglist', '$stateParams', 'Feedback', 'WaitLock',
                            function ($scope, Hijackinglist, $stateParams, Feedback, WaitLock){
    $scope.orgNr = $stateParams.orgNr;

    $scope.entries = [];

    $scope.editing = false;
    var editOrigTmp = null;
    $scope.edit = function(entry){
        if($scope.editing)return;
        entry.editing = $scope.editing = true;
        editOrigTmp = angular.copy(entry);
    };
    var quitEditing = function(entry){
        editOrigTmp = null;
        entry.editing = $scope.editing = false;
    };
    $scope.undoEdit = function(entry, form){
        if(WaitLock.check())return;
        if(entry.editing){
            angular.copy(editOrigTmp, entry);
            quitEditing(entry);
            form.$setPristine();
            form.$setUntouched();
        }
    };

    var loadHijackingList = function(){
        Feedback.http('hijackinglist.get', Hijackinglist.getEntries($scope.orgNr), {
            403: 'Ej behörig att se kapningslistan',
            '4XX': 'Kapningslistan kunde inte hämtas',
            '5XX': 'Ett fel inträffade. Kapningslistan kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.entries = res.data;
        });
    };
    loadHijackingList();

    $scope.save = function(entry, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('hijackinglist.update', Hijackinglist.updateEntry(entry), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra uppgifterna',
            '4XX': 'Kapningslistan kunde inte ändras',
            '5XX': 'Ett fel inträffade. Kapningslistan kunde inte ändras. Försök igen senare.'
        })
        .then(function(){
           quitEditing(entry);
           loadHijackingList();
           form.$setPristine();
           form.$setUntouched();
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.remove = function(entry){
        if(WaitLock.check())return;
        Feedback.http('hijackinglist.delete', Hijackinglist.deleteEntry(entry), {
            403: 'Ej behörig att radera uppgift',
            '4XX': 'Uppgifterna kunde inte raderas',
            '5XX': 'Ett fel inträffade. Uppgifterna kunde inte raderas. Försök igen senare.'
        })
        ['finally'](function(){
            loadHijackingList();
        });
    };
}]);
