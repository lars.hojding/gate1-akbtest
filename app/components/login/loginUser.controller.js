angular.module('app.controllers')
.controller('LogInUserCtrl', ['$scope', '$state', 'Authentication', '$sce', 'Feedback', '$window', '$location', 'WaitLock',
                function($scope, $state, Authentication, $sce, Feedback, $window, $location, WaitLock){
    $scope.$state = $state;
    $scope.grandidsession = $state.params.grandidsession;
    $scope.showBackdoor = false;


    if(typeof $scope.grandidsession !== 'undefined' && $scope.grandidsession !== null){
        if(WaitLock.lock())return;
	    Authentication.bankIdFinish($scope.grandidsession)
	    .then(function(res){
	        $state.go('user.dashboard',
	        	{orgnr: res.queryParams.orgnr, 
	    		email: res.queryParams.email, 
	    		phone: res.queryParams.phone,
	    		firstName: res.queryParams.firstName,
	    		lastName: res.queryParams.lastName,
	    		clientType: res.queryParams.clientType});
	    }, function(err){
	        $scope.resetBankId();
	        if(err.errorCode === 'UNKNOWN_ERROR'){
	            Feedback.clearTarget('login.bankid')
	            .add('login.bankid', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
	        }else{
	            var msg =  getBankIDStatusMessages(err.errorCode, 'm');
	            Feedback.clearTarget('login.bankid').add('login.bankid', Feedback.types.FAILURE, 9999, msg);
	            $location.url($location.path());
	        }
	    }, function(notifyValue){
	        $scope.bankId.statusMsg = getBankIDStatusMessages(notifyValue.progressStatus, 'm');
	    })
	    ['finally'](function(){
	        WaitLock.unlock();
	    });
    }
    else{
    Authentication.isTestEnv().then(function(){
        $scope.showBackdoor = true;
    });
    }
    $scope.isMobileBrowser = function(){
        var check = false;
        (
            function(a,b){
                if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
                    check = true;
                }
        )(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    };

    $scope.bankId = {
        state: 'chose',
        openApp: false,
        showLink: false,
        statusMsg: '',
        link: '',
        numNotify: 0
    };

    $scope.$watch('bankId.state', function(state){
        if(state != 'chose')
            Feedback.clearTarget('login.bankid');
    });

    $scope.resetBankId = function(){
        $scope.bankId.state = 'chose';
        $scope.bankId.statusMsg = '';
        $scope.bankId.numNotify = 0;
        $scope.bankId.openApp = false;
    };

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    };

    var getBankIDStatusMessages = function(status, variant){
        var msgList = {
            'RFA1':   'Starta BankID-programmet.',
            'RFA2':   'Du har inte BankID-appen installerad. Kontakta din bank.',
            'RFA3':   'Åtgärden avbruten. Försök igen.',
            'RFA5':   'Internt tekniskt fel. Försök igen.',
            'RFA6':   'Åtgärden avbruten.',
            'RFA8':   'BankID-programmet svarar inte. Kontrollera att det är startat och att du har internetanslutning. Försök sedan igen.',
            'RFA9':   'Skriv in din säkerhetskod i BankID-programmet och välj Legitimera eller Skriv under.',
            'RFA12':  'Internt tekniskt fel. Uppdatera BankID-programmet och försök igen.',
            'RFA13':  'Försöker starta BankID-programmet',
            'RFA14A': 'Du har inget BankID som går att använda för den här inloggningen/underskriften på den här datorn. Om du har ett BankIDkort, sätt in det i läsaren. Om du inte har något BankID kan du hämta ett hos din Bank. Om du har ett BankID på en annan enhet kan du starta ditt BankID-program där',
            'RFA14B': 'Du har inget BankID som går att använda för den här inloggningen/underskriften på den här enheten. Om du inte har något BankID kan du hämta ett hos din Bank. Om du har ett BankID på en annan enhet kan du starta ditt BankID-program där.',
            'RFA15A': 'Du har inget BankID som går att använda för den här inloggningen/underskriften på den här datorn. Om du har ett BankIDkort, sätt in det i läsaren. Om du inte har något BankID kan du hämta ett hos din Bank.',
            'RFA15B': 'Du har inget BankID som går att använda för den här inloggningen/underskriften på den här enheten. Om du inte har något BankID kan du hämta ett hos din Bank.',
            'RFA16':  'Det BankID du försöker använda är för gammalt eller spärrat. Använd ett annat BankID eller hämta ett nytt hos din bank.',
            'RFA17':  'BankID-programmet verkar inte finnas i din dator eller telefon. Installera det och hämta ett BankID hos din bank. Installera programmet från install.bankid.com.',
            'RFA18':  'Starta BankID-programmet',
            'RFA19':  'Vill du logga in eller skriva under med BankID på den här datorn eller med ett Mobilt BankID?',
            'RFA20':  'Vill du logga in eller skriva under med ett BankID på den här enheten eller med ett BankID på en annan enhet?'
        };
        var local = {
            'OUTSTANDING_TRANSACTION': 'RFA13',
            'NO_CLIENT': 'RFA1',
            'STARTED': 'RFA15A',
            'USER_SIGN': 'RFA9',
            // Error codes
            'INTERNAL_ERROR': 'RFA5',
            'RETRY': 'RFA5',
            'CLIENT_ERR': 'RFA12',
            'CERTIFICATE_ERR': 'RFA16',
            'USER_CANCEL': 'RFA6',
            'CANCELLED': 'RFA3',
            'START_FAILED': 'RFA17',
            'EXPIRED_TRANSACTION': 'FRA8'
        };
        var mobile = angular.copy(local);
        mobile['OUTSTANDING_TRANSACTION'] = 'RFA1';
        mobile['STARTED'] = 'RFA14B';

        var l = null;
        if(variant === 'l') l = local;
        else if(variant === 'm') l = mobile;
        else return;

        if(typeof l[status] === 'string' && typeof msgList[l[status]] === 'string'){
            return msgList[l[status]];
        }
        return '';
    };

    $scope.logInBankID = function(personalNumber){
        if(WaitLock.lock())return;
        $scope.bankId.statusMsg = getBankIDStatusMessages('OUTSTANDING_TRANSACTION', 'l');
        $scope.bankId.state = 'bankid_status';
        Authentication.bankIdStart(personalNumber)
        .then(function(startRes){
        	$window.location.href=startRes.data.redirectUrl;
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.cancelBankIDLogin = function(){
        Authentication.cancelLogin();
    };

}]);
