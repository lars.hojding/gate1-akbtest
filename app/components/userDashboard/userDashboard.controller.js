angular.module('app.controllers')
.controller('UserDashboardCtrl', ['$scope','$state', 'currUser', 'UserHandler', 'Subscriptions', 'Registration', 'Feedback', 'WaitLock',
                    function($scope, $state, currUser, UserHandler, Subscriptions, Registration, Feedback, WaitLock){
    $scope.state = $state;
    $scope.subs = null;
    $scope.searchingCompany = false;
    $scope.gettingData = false;
    $scope.form = {};
    $scope.subscriptionsPerPage = 6;
   
    
    if(typeof currUser == 'object'){
        $scope.isActiveUser = true;
        $scope.isMultiUser = currUser.isMultiUser;
        $scope.isSystemUser = currUser.isSystemUser;
        $scope.isFortnoxUser = currUser.isFortnoxUser;
        $scope.termsAccepted = currUser.termsAccepted;
	}

    var paramOrgnr = $state.params.orgnr;

    Subscriptions.getUserSubscriptions(paramOrgnr)
    .then(function(res){
        $scope.subs = res.data;
        if(paramOrgnr != null){
        	paramOrgnr = paramOrgnr.replace('-','');
	        var result = $scope.subs.filter(function(obj){
	        	return obj.orgNr == paramOrgnr;
	        });
	        if(typeof result == 'undefined' || result.length < 1){
	        	$scope.form.getDataOrgnr = paramOrgnr;
	        	getData(paramOrgnr);
	        }
	        else if($scope.isAB(result[0])){
	    		$state.go('user.company.company.ab', {orgNr: paramOrgnr});
	    	}
	    	else if($scope.isHB(result[0])){
	    		$state.go('user.company.company.hb', {orgNr: paramOrgnr});
	    	}
        }
    });
    
    $scope.isAB = function(subscription){
        return subscription.companyType == 1;
    };

    $scope.isHB = function(subscription){
        return subscription.companyType == 3;
    };

    $scope.acceptTerms = function() {
    	if(WaitLock.lock()) return;
    	Feedback.http(
    	'user.accept',
    	UserHandler.acceptTerms(currUser.userId),
    	{
    		'4XX': 'Kan inte uppdatera användare. Kontrollera dina behörigheter.',
    		'5XX': 'Ett fel inträffade. Försök igen senare.'
    	})
    	.then(function(res) {
    		currUser.termsAccepted = true;
    		$scope.termsAccepted = true;
    	},function(err){
    		if(err.errorCode == 'UNKNOWN_ERROR'){
                Feedback.clearTarget('user.accept')
                .add('user.accept', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    		}
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    	});
    };
    
    $scope.searchCompany = function(){
    	$scope.searchingCompany = false;
    	$scope.companyNotFound = false;
    	var search = $scope.form.companyForm.search;
    	if(search.$pristine || search.$invalid){
    		search.$setDirty();
    		search.$setTouched();
    		return;
    	}
    	else {
    		getCompany();
    	}
    };
    var getData = function(getDataOrgnr){
    	if(WaitLock.lock()) return;
    	$scope.gettingData = true;
    	Feedback.http(
    	'company.getData',
    	Registration.fetchData(getDataOrgnr),
    	{
    		'4XX': 'Kunde inte hämta bolag. Kontrollera dina behörigheter.',
    		'5XX': 'Ett fel inträffade. Kunde inte hämta bolag. Försök igen senare.'
    	})
    	.then(function(res) {
    		if(res.status == 204){
    			Feedback.clearTarget('company.getData')
    			.add('company.getData', Feedback.types.FAILURE, 204, 'Uppgifter om bolaget finns inte. Kontakta kundtjänst om problemet kvarstår.');
    		}
    		else if(res.status == 205){
    			Feedback.clearTarget('company.getData')
    			.add('company.getData', Feedback.types.FAILURE, 205, 'Bolaget finns redan i Aktieboken.');    			
    		}
    		else if(typeof res.data == 'object') {
    			if(res.data.companyType == 'ab' || res.data.companyType == 'hk'){
    				$state.go('user.registration', {
    					basicData: res.data, 
    					email: $state.params.email, 
    					phone: $state.params.phone, 
    					firstName: $state.params.firstName,
    					lastName: $state.params.lastName,
    					clientType: $state.params.clientType,
    					termsAccepted: $scope.termsAccepted});
	    		}
	    		else {
	    			Feedback.clearTarget('company.getData')
	    			.add('company.getData', Feedback.types.FAILURE, 205, 'Det går bara att lägga till Aktiebolag eller Handelsbolag.');
	    		}
    		}
    		else {
    			$scope.gettingData = false;
    		}
    	},
    	function(err) {
    		if(err.errorCode == 'UNKNOWN_ERROR'){
                Feedback.clearTarget('company.getData')
                .add('company.getData', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
    		}
    	})
    	['finally'](function(){
    		$scope.gettingData = false;
    		WaitLock.unlock();
    	});
    };

    

    
    $scope.fetchData = function() {
    	$scope.gettingData = false;
    	var search = $scope.form.getDataForm.fetch;
    	if(search.$pristine || search.$invalid){
    		search.$setDirty();
    		search.$setTouched();
    		return;
    	}
    	else {
    		getData($scope.form.getDataOrgnr);
    	}    	
    };
    
    var getCompany = function(){
    	if (WaitLock.lock()) return;
    	$scope.searchingCompany = true;
    	var searchOrgnr = $scope.form.searchOrgnr;
    	Feedback.http(
    	'company.search',
    	Subscriptions.getCompanySubscription(searchOrgnr),
    	{
    		'4XX': 'Kunde inte hämta bolag. Kontrollera dina behörigheter.',
    		'5XX': 'Ett fel inträffade. Kunde inte hämta bolag. Försök igen senare.'	
    	})
    	.then(function(res){
    		if(res.status == 204){
    			Feedback.clearTarget('company.search')
    			.add('company.search', Feedback.types.FAILURE, 205, 'Bolaget finns inte i Aktieboken.');    			
    		}
    		else if($scope.isAB(res.data)){
    			$state.go('user.company.company.ab', {orgNr: searchOrgnr});
    		}
    		else if($scope.isHB(res.data)){
    			$state.go('user.company.company.hb', {orgNr: searchOrgnr});
    		}
    		else{
    			$scope.searchingCompany = false;
    		}
    	},
    	function(err){
    		if(err.errorCode === 'UNKNOWN_ERROR'){
                Feedback.clearTarget('company.search')
                .add('company.search', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
            }else{
                Feedback.clearTarget('company.search').add('company.search', Feedback.types.FAILURE, 9999, "Kunde inte hämta bolag. Försök senare.");
            }    		
    	})
        ['finally'](function(){
            $scope.searchingCompany = false;
            WaitLock.unlock();
        });   		
    };
}]);
