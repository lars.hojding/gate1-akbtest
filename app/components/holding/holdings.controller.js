angular.module('app.controllers')
.controller('HoldingsCtrl', ['$scope', 'Holding', '$stateParams', 'Feedback', 'companyType', 'WaitLock',
                function ($scope, Holding, $stateParams, Feedback, companyType, WaitLock){
    $scope.companyType = companyType;
    $scope.orgNr = $stateParams.orgNr;
    $scope.editing = false;
    $scope.holdingsPerPage = 10;

    $scope.holdings = [];

    $scope.compareNumber = function(holding){
        return parseInt(holding.fromNumberSerie);
    };

    if($scope.companyType === 'AB'){
        /*
         * AB
         */
        var loadHoldings = function(){
            Feedback.http('holding.ab.get', Holding.ab.getHoldings($scope.orgNr), {
                '4XX': 'Aktieboken kunde inte hämtas.',
                '5XX': 'Ett fel inträffade. Aktieboken kunde inte hämtas. Försök igen senare.'
            })
            .then(function(res){
                $scope.holdings = res.data;
            });
        };
    }else if($scope.companyType === 'HB'){
        var loadHoldings = function(){
            Feedback.http('holding.hb.get', Holding.hb.getHoldings($scope.orgNr), {
                '4XX': 'Ägarförteckning kunde inte hämtas.',
                '5XX': 'Ett fel inträffade. Ägarförteckning kunde inte hämtas. Försök igen senare.'
            })
            .then(function(res){
                $scope.holdings = res.data;
            }, function(err){
                // TODO: Error handling
            });
        };
    }

    /*
     * HB only
     */
    var editOrigTmp = null;
    $scope.edit = function(holding){
        if($scope.editing)return;
        holding.editing = $scope.editing = true;
        editOrigTmp = angular.copy(holding);
    };
    var quitEditing = function(holding, form){
        editOrigTmp = null;
        holding.editing = $scope.editing = false;
        form.$setPristine();
        form.$setUntouched();
    };
    $scope.undoEdit = function(holding, form){
        if(WaitLock.check())return;
        if(holding.editing){
            angular.copy(editOrigTmp, holding);
            quitEditing(holding, form);
        }
    };

    $scope.save = function(holding, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('holding.hb.update', Holding.hb.updateHolding(holding), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra uppgifter',
            '4XX': 'Uppgifterna kunde inte ändras',
            '5XX': 'Ett fel inträffade. Uppgifterna kunde inte ändras. Försök igen senare.'
        })
        .then(function(){
           quitEditing(holding, form);
           loadHoldings();
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.remove = function(holding){
        if(WaitLock.check())return;
        Feedback.http('holding.hb.delete', Holding.hb.deleteHolding(holding), {
            403: 'Ej behörig att radera uppgifter',
            '4XX': 'Uppgifterna kunde inte raderas',
            '5XX': 'Ett fel inträffade. Uppgifterna kunde inte raderas. Försök igen senare.'
        })
        ['finally'](function(){
            loadHoldings();
        });
    };

    loadHoldings();
}]);
