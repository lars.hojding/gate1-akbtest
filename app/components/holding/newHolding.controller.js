angular.module('app.controllers')
.controller('NewHoldingCtrl', ['$scope', 'Holding', '$stateParams', '$state', 'Feedback', 'WaitLock', '$timeout', 'Owner',
                function ($scope, Holding, $stateParams, $state, Feedback, WaitLock, $timeout, Owner){
    $scope.orgNr = $stateParams.orgNr;

    $scope.getInputOwnerId = function(){
    	if($stateParams.foreignOwner){
    		return $stateParams.foreignOwner.ownerId;
    	}
        if(typeof $stateParams.ownerId === 'undefined')return '';
        if(isNaN(parseInt($stateParams.ownerId)))return '';
        var a = parseInt($stateParams.ownerId);
        if(a > 0)return a;
        return '';
    };

    
    $scope.foreignOwner = {};
    if($stateParams.foreignOwner){
    	$scope.foreignOwner=$stateParams.foreignOwner;
    }
    
    $scope.holding = {
      "orgNr": $scope.orgNr,
      "ownerId": $scope.getInputOwnerId(),
      "ownershipInPercent": 0,
      "ownerName": "",
      "firma": ""
    };

    $scope.hasValidOwner = false;
    $scope.checkingOwner = false;

    var checkOwnerExist = function(){
        $scope.checkingOwner = true;
        $timeout(function(){
            Owner.exists($scope.holding.ownerId, $scope.orgNr)
            .then(function(exists){
                if(exists.exists){
                    // Owner exists - show the rest of the form
                    if(typeof exists.owner.firstName == 'string' && exists.owner.firstName.length > 0){
                        $scope.holding.ownerName = exists.owner.firstName;
                        if(typeof exists.owner.lastName == 'string' && exists.owner.lastName.length > 0){
                            $scope.holding.ownerName += ' ' + exists.owner.lastName;
                        }
                    }else if(typeof exists.owner.lastName == 'string' && exists.owner.lastName.length > 0){
                        $scope.holding.ownerName = exists.owner.lastName;
                    }
                    $scope.checkingOwner = false;
                    $scope.hasValidOwner = true;
                }else{
                    // Owner does not exist - go to owner registration
                    $state.go('user.company.owner.newFromHolding', {orgNr: $scope.orgNr, ownerId: $scope.holding.ownerId});
                }
            });
        }, 30);
    };

    $scope.checkOwner = function(form){
        var formOwner = form.ownerId;
        if(formOwner.$pristine || formOwner.$invalid){
            formOwner.$setDirty();
            formOwner.$setTouched();
            return;
        }
        checkOwnerExist();
    };

    $scope.checkForeignOwner = function(){
    	$scope.holding.ownerId = $scope.foreignOwner.ownerId;
    	$scope.holding.ownerName = $scope.foreignOwner.firstName + ' ' + $scope.foreignOwner.lastName;
    	$scope.checkingOwner = false;
    	$scope.hasValidOwner = true;
    };

    $scope.newForeignOwner = function(){
        $state.go('user.company.owner.newForeignOwnerFromHolding', {orgNr: $scope.orgNr});
    };
    
    if($scope.getInputOwnerId()){
        checkOwnerExist();
    }

    $scope.save = function(){
        var form = $scope.holdingForm;
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('holding.create').add('holding.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla fälten');
            }else{
                Feedback.clearTarget('holding.create').add('holding.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('holding.create', Holding.hb.createHolding($scope.holding), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till innehav',
            409: 'Uppgifterna finns redan tillagda',
            '4XX': 'Delegaten kunde inte läggas till',
            '5XX': 'Ett fel inträffade. Delegaten kunde inte läggas till. Försök igen senare.'
        })
        .then(function(){
           $state.go('user.company.holding.listhb', {orgNr: $scope.orgNr});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };
    
    if($scope.foreignOwner && $scope.foreignOwner.foreign){
    	$scope.checkForeignOwner();
    };
    
}]);
