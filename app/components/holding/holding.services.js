angular.module('app.services')
.factory('Holding', ['AuthHttp', function(AuthHttp){
    return {
        hb: {
            getHoldings: function(orgNr){
                return AuthHttp.get('holding/hb', {params: {orgNr: orgNr}});
            },
            updateHolding: function(holdingData){
                return AuthHttp.put('holding/hb', holdingData);
            },
            createHolding: function(holdingData){
                return AuthHttp.post('holding/hb', holdingData);
            },
            deleteHolding: function(holdingData){
                return AuthHttp['delete']('holding/hb', {params: {
                    orgNr: holdingData.orgNr, ownerId: holdingData.ownerId}});
            }
        },
        ab: {
            getHoldings: function(orgNr){
                return AuthHttp.get('holding', {params: {orgNr: orgNr}});
            }
        },
        companygroup : {
        	getRecords: function(orgNr){
        		return AuthHttp.get('holding/companyGroupData', {params: {orgNr: orgNr}});
        	},
        	updateRecord: function(groupData){
        		return AuthHttp.put('holding/companyGroupData', groupData);
        	},
        	createRecord: function(groupData){
        		return AuthHttp.post('holding/companyGroupData', groupData);
        	}
        }
    };
}]);
