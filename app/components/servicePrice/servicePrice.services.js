angular.module('app.services')
.factory('ServicePrice', ['UserHandler', function(UserHandler){
    var PRICE_STANDARD = 149;
    return function(){
    	return UserHandler.getSelf().then(function(res){
    		if(typeof res.data == 'object' && res.data.isMultiUser == true){
    			return res.data.price;
            }else{
                return PRICE_STANDARD;
            }
        }, function(){
            return PRICE_STANDARD;
        });
    };
}]);
