angular.module('app.services')
.factory('Feedback', ['$q', '$filter', function($q, $filter){
    var targets = {};
    var allTypes = {
        SUCCESS: 0x1,
        FAILURE: 0x2,
        WARNING: 0x4,
        ERROR:   0x8,
        UNKNOWN: 0x10
    };
    var prepareTarget = function(target){
        if(typeof targets[target] === 'undefined' || targets[target] === null){
            targets[target] = {
                feedback: []
            };
        }else if(typeof targets[target].feedback === 'undefined'){
            targets[target].feedback = [];
        }
    };
    var isString = function(s){
        return typeof s === 'string';
    };
    var isDefined = function(s){
        return !(typeof s === 'undefined' || s === null);
    };
    var getValidType = function(type){
        if(type === allTypes.SUCCESS
                || type === allTypes.FAILURE
                || type === allTypes.WARNING
                || type === allTypes.ERROR){
            return type;
        }
        return allTypes.UNKNOWN;
    };
    return {
        types: allTypes,
        add: function(target, type, code, msg, customData){
            if(!isString(target) || (isString(code)))return this;
            var _type = getValidType(type);
            prepareTarget(target);
            var newEntry = {
                type: _type,
                code: code,
                message: isString(msg) ? msg : '',
                remove: function(){
                    var index = 0;
                    for(;index < targets[target].feedback.length; index++){
                        if(targets[target].feedback[index] === this){
                            targets[target].feedback.splice(index, 1);
                            break;
                        }
                    }
                }
            };
            if(typeof customData !== 'undefined')newEntry.customData = customData;
            targets[target].feedback.push(newEntry);
            return this;
        },
        entries: function(target, type){
            prepareTarget(target);
            return targets[target].feedback;
        },
        hasCode: function(target, code){
            return false;
        },
        clearTarget: function(target){
            prepareTarget(target);
            targets[target].feedback.length = 0;
            return this;
        },
        clearAll: function(){
            return this;
            if(typeof targets !== 'object')return;
            for(prop in targets){
                if(targets.hasOwnProperty(prop)){
                    delete obj[prop];
                }
            }
            return this;
        },
        http: function(target, promise, msgMapper){
            var _this = this;
            var getMsg = function(code){
                if(typeof msgMapper !== 'object')return '';
                var msg = '';
                if(typeof msgMapper[code] === 'string'){
                    msg = msgMapper[code];
                }else{
                    if(code >= 500 && code < 600){
                        if(typeof msgMapper['5XX'] === 'string')msg = msgMapper['5XX'];
                    }else if(code >= 400 && code < 500){
                        if(typeof msgMapper['4XX'] === 'string')msg = msgMapper['4XX'];
                    }
                }

                return msg;
            };
            return promise.then(function(resSuccess){
                _this.clearTarget(target);
                if(typeof resSuccess != 'object' || typeof resSuccess.status == 'undefined'){
                    _this.add(target, allTypes.UNKNOWN, 0, '', resSuccess);
                }else{
                    _this.add(target, allTypes.SUCCESS, resSuccess.status, getMsg(resSuccess.status), resSuccess);
                }
                return resSuccess;
            }, function(resError){
                _this.clearTarget(target);
                if(typeof resError != 'object' || typeof resError.status == 'undefined'){
                    _this.add(target, allTypes.UNKNOWN, 0, '', resError);
                } else if(resError.status >= 400 && resError.status < 405){
                    _this.add(target, allTypes.FAILURE, resError.status, getMsg(resError.status), resError);
                }else if(resError.status >= 405){
                    _this.add(target, allTypes.ERROR, resError.status, getMsg(resError.status), resError);
                }else{
                    _this.add(target, allTypes.UNKNOWN, resError.status, getMsg(resError.status), resError);
                }
                return $q.reject(resError);
            });
        }
    }
}]);
