angular.module('app.directives')
.directive('svabFeedbackTarget', ['Feedback', '$filter', '$timeout', '$location', '$anchorScroll',
            function(Feedback, $filter, $timeout, $location, $anchorScroll){
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        template:
               '<div class="feedback-container" id="{{thisID}}">'
            +    '<div class="feedback-entry-container" ng-class="getTypeClass(entry)" ng-repeat="entry in entries" ng-if="entry.message !==\'\'">'
            +        '<span class="feedback-entry-message">{{entry.message}}</span>'
            +        '<div class="feedback-close" ng-click="entry.remove()"><i class="fa fa-times"></i></div>'
            +    '</div>'
            +       '<div class="feedback-entry-container" ng-class="feedback-default" ng-show="showDefault()" ng-transclude></div>'
            +   '</div>',
        scope: {
            target: '@svabFeedbackTarget',
            type:   '@svabFeedbackType',
            testFailure:   '@svabFeedbackTestFailure'
        },
        controller: ['$scope', function($scope){
            $scope.hasDefault = false;
            this.enableDefaultMsg = function(){
                $scope.hasDefault = true;
            };
        }],
        link: function(scope, element, attrs){
            scope.thisID = 'feedbacktarget' + scope.$id;
            scope.entries = [];
            scope.getEntries = function(target){
                scope.entries = Feedback.entries(target);
            };
            scope.getTypeClass = function(entry){
                switch(entry.type){
                    case Feedback.types.SUCCESS: return 'feedback-success';
                    case Feedback.types.FAILURE: return 'feedback-failure';
                    case Feedback.types.WARNING: return 'feedback-warning';
                    case Feedback.types.ERROR: return 'feedback-error';
                    case Feedback.types.UNKNOWN: return 'feedback-unknown';
                }
            };

            scope.showDefault = function(){ return scope.entries.length == 0
                    && scope.hasDefault === true;
            };

            scope.entriesAll = Feedback.entries(scope.target);

            var type = null;
            if(typeof scope.type == 'string'){
                type = Feedback.types.UNKNOWN;
                switch(scope.type.toUpperCase()){
                    case 'S': case 'SUCCESS': type = Feedback.types.SUCCESS; break;
                    case 'F': case 'FAILURE': type = Feedback.types.FAILURE; break;
                    case 'W': case 'WARNING': type = Feedback.types.WARNING; break;
                    case 'E': case 'ERROR': type = Feedback.types.ERROR; break;
                }
            }

            if(type !== null){
                scope.$watch(function(){
                    return scope.entriesAll;
                }, function(n){
                    scope.entries = $filter('filter')(n, function(v){
                        return v.type === type;
                    });
                }, true);
            }else{
                scope.entries = scope.entriesAll;
            }

            var numEntries = 0;
            scope.$watch('entries', function(newValue){
                var newValueLength = $filter('filter')(newValue, function(v){
                     return v.message !== '';
                 }).length;
                if(newValueLength > numEntries){
                    $location.hash(scope.thisID);
                    $anchorScroll();
                }
                numEntries = newValueLength;
            }, true);


            // TEST
            if(typeof scope.testFailure === 'string' && scope.testFailure != ''){
                $timeout(function(){
                    Feedback.add(scope.target, Feedback.types.FAILURE, 400, scope.testFailure);
                }, 1500);
            }
        }
    };
}])

.directive('svabFeedbackDefault', [function(){
    return {
        transclude: true,
        replace: true,
        restrict: 'E',
        require: '^svabFeedbackTarget',
        template: '<div ng-transclude></div>',
        link: function(scope, element, attrs, feedbackCtrl){
            feedbackCtrl.enableDefaultMsg(scope);
        }
    };
}]);
