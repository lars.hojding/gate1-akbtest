angular.module('app.services')
.factory('Registration', ['AuthHttp', function(AuthHttp){
    return {
        registerAB: function(formData){
            return AuthHttp.post('registration/registerAB', formData);
        },
        registerHB: function(formData){
            return AuthHttp.post('registration/registerHB', formData);
        },
        fetchData: function(orgNr){
        	return AuthHttp.get('registration/fetchData', {params:{orgNr: orgNr}});
        }
    };
}]);
