angular.module('app.controllers')
.controller('RegistrationCtrl', ['$scope', '$stateParams', 'Registration', '$state', 'Feedback', 'ServicePrice', 'WaitLock',
                function($scope, $stateParams, Registration, $state, Feedback, ServicePrice, WaitLock){
    var basicData = $stateParams.basicData;
    var email = $stateParams.email;
    var phone = $stateParams.phone;
    var firstName = $stateParams.firstName;
    var lastName = $stateParams.lastName;
    var clientType = $stateParams.clientType;
    var termsAccepted = $stateParams.termsAccepted;

	$scope.subscription = {
        "fromDate": "",
        "toDate": "",
        "serviceTypeId": 1,
        "companyName": basicData.firma,
        "isOwner": false,
        "isDelegate": false,
        "firstName": "",
        "lastName": "",
        "contactDetails": {
            "phoneNumber": "",
            "email": ""
        },
        "orderDate": "",
        "companyType": basicData.companyType == 'ab'? 1 : 3,

        "hbBasicInfo": {
            "orgNr": "",
            "firma": "",
            "contactFirstName": "",
            "contactLastName": "",
            "contactDetails": {
                "phoneNumber": "",
                "email": ""
            },
            "address": {
                "cOAddress": "",
                "streetAddress": "",
                "zipCode": "",
                "city": "",
                "country": ""
            }
        },
        "abBasicInfo": {
            "orgNr": "",
            "firma": "",
            "contactFirstName": "",
            "contactLastName": "",
            "contactDetails": {
                "phoneNumber": "",
                "email": ""
            },
            "preEmption": false,
            "address": {
                "cOAddress": "",
                "streetAddress": "",
                "zipCode": "",
                "city": "",
                "country": ""
            },
            "shareCapital": 0,
            "currency": "",
            "totalShares": 0
        },
        "shareInfos": [
            /*{
                "orgNr": "",
                "aktieSlag": "",
                "antal": 0,
                "rostVarde": ""
            }*/
        ]
    };

    if(basicData.companyType == 'ab'){
    	$scope.subscription.companyType = 1;
    	$scope.subscription.abBasicInfo.orgNr = basicData.orgNr;
    	$scope.subscription.abBasicInfo.firma = basicData.firma;
    	$scope.subscription.abBasicInfo.shareCapital = isNaN(Number(basicData.shareCapital))? "" : Number(basicData.shareCapital);
    	$scope.subscription.abBasicInfo.currency = basicData.currency;
    	$scope.subscription.abBasicInfo.totalShares = isNaN(Number(basicData.totalShares))? "" : Number(basicData.totalShares);
    	$scope.subscription.abBasicInfo.address.streetAddress = basicData.postalStreetAddress;
    	$scope.subscription.abBasicInfo.address.zipCode = basicData.postalZipCode;
    	$scope.subscription.abBasicInfo.address.city = basicData.postalCity;
    	if(basicData.shares && basicData.shares.length > 0){
    		for (var i = 0 ; i < basicData.shares.length; ++i){
    			var shareType = {};
    			shareType.orgNr = basicData.shares[i].orgNr;
    			shareType.aktieSlag = basicData.shares[i].name;
    			shareType.antal = isNaN(Number(basicData.shares[i].number))? "" : Number(basicData.shares[i].number);
    			shareType.rostVarde = basicData.shares[i].votes;
    			$scope.subscription.shareInfos.push(shareType);
    		}
    	}
    	if(email != null){
    		$scope.subscription.abBasicInfo.contactDetails.email = email;
    	}
    	if(phone != null){
    		$scope.subscription.abBasicInfo.contactDetails.phoneNumber = phone;
    	}
    	if(firstName != null){
    		$scope.subscription.abBasicInfo.contactFirstName = firstName;
    	}
    	if(lastName != null){
    		$scope.subscription.abBasicInfo.contactLastName = lastName;
    	}
    }
    else if(basicData.companyType == 'hk'){
    	$scope.subscription.companyType = 3;
    	$scope.subscription.hbBasicInfo.orgNr = basicData.orgNr;
    	$scope.subscription.hbBasicInfo.firma = basicData.firma;
    	$scope.subscription.hbBasicInfo.address.streetAddress = basicData.postalStreetAddress;
    	$scope.subscription.hbBasicInfo.address.zipCode = basicData.postalZipCode;
    	$scope.subscription.hbBasicInfo.address.city = basicData.postalCity;    	
    	if(email != null){
    		$scope.subscription.hbBasicInfo.contactDetails.email = email;
    	}
    	if(phone != null){
    		$scope.subscription.hbBasicInfo.contactDetails.phoneNumber = phone;
    	}
    	if(firstName != null){
    		$scope.subscription.hbBasicInfo.contactFirstName = firstName;
    	}
    	if(lastName != null){
    		$scope.subscription.hbBasicInfo.contactLastName = lastName;
    	}
    }
    
    $scope.agreement = {
    		agree: false
    };

    $scope.agreement.agree = termsAccepted || clientType != null || (email != null && phone != null && firstName != null && lastName != null);

    $scope.saveSubscription = function(form){
        if(!$scope.subscription.hasAdmin){
            if($scope.subscription.companyType == 1){
                $scope.subscription.firstName = $scope.subscription.abBasicInfo.contactFirstName;
                $scope.subscription.lastName = $scope.subscription.abBasicInfo.contactLastName;
                $scope.subscription.contactDetails.phoneNumber = $scope.subscription.abBasicInfo.contactDetails.phoneNumber;
                $scope.subscription.contactDetails.email = $scope.subscription.abBasicInfo.contactDetails.email;
            }else if($scope.subscription.companyType == 3){
                $scope.subscription.firstName = $scope.subscription.hbBasicInfo.contactFirstName;
                $scope.subscription.lastName = $scope.subscription.hbBasicInfo.contactLastName;
                $scope.subscription.contactDetails.phoneNumber = $scope.subscription.hbBasicInfo.contactDetails.phoneNumber;
                $scope.subscription.contactDetails.email = $scope.subscription.hbBasicInfo.contactDetails.email;
            }
        }

        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('subscriptions.create').add('subscriptions.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('subscriptions.create').add('subscriptions.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        var regType = null;
        if($scope.subscription.companyType == 1){
            regType = Registration.registerAB;
            $scope.subscription.companyName = $scope.subscription.abBasicInfo.firma;
            $scope.subscription.orgNr= $scope.subscription.abBasicInfo.orgNr;
        }else if($scope.subscription.companyType == 3){
            regType = Registration.registerHB;
            $scope.subscription.companyName = $scope.subscription.hbBasicInfo.firma;
            $scope.subscription.orgNr= $scope.subscription.hbBasicInfo.orgNr;
        }

        for(var i = 0; i < $scope.subscription.shareInfos.length; ++i){
            if(($scope.subscription.shareInfos[i].aktieSlag + '') === ''
                    && $scope.subscription.shareInfos[i].antal == 0
                    && ($scope.subscription.shareInfos[i].rostVarde + '') === ''){
                $scope.subscription.shareInfos.splice(i, 1);
                i--;
            }else{
                $scope.subscription.shareInfos[i].orgNr = $scope.subscription.companyType == 1
                    ? $scope.subscription.abBasicInfo.orgNr
                    : $scope.subscription.hbBasicInfo.orgNr;
            }
        }

        ServicePrice().then(function(price){
            $scope.subscription.servicePrice = price;
            if(regType !== null){
                if(WaitLock.lock())return;
                Feedback.http('subscriptions.create', regType($scope.subscription), {
                    400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
                    403: 'Ej behörig att starta prenumeration',
                    409: 'Företaget är redan tillagt',
                    '4XX': 'Kunde inte starta prenumeration',
                    '5XX': 'Ett fel inträffade. Kunde inte starta prenumeration. Försök igen senare.'
                })
                .then(function(){
                    WaitLock.unlock();
                    $state.go(
                        $scope.subscription.companyType == 1
                            ? 'user.company.company.ab'
                            : 'user.company.company.hb', {orgNr: $scope.subscription.orgNr});
                })
                ['finally'](function(){
                    WaitLock.unlock();
                });
            }
        });
    };
}]);
