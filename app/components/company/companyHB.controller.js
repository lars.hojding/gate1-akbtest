angular.module('app.controllers')
.controller('CompanyHBCtrl', ['$scope', 'Company', 'Holding', '$stateParams', 'Feedback', '$state', 'WaitLock',
            function ($scope, Company, Holding, $stateParams, Feedback, $state, WaitLock){
    $scope.orgNr = $stateParams.orgNr;
    $scope.company = {};
    $scope.company.editing = false;

    $scope.companyDataBackup = null;

    var loadCompany = function(){
        Feedback.http('company.hb.get', Company.getHB($scope.orgNr), {
            '4XX': 'Information om företaget kunde inte hämtas.',
            '5XX': 'Ett fel inträffade. Information om företaget kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.companyDataBackup = angular.copy(res.data);
            $scope.company = res.data;
        }, function(err){
            // TODO: Error handling
        });
    };
    loadCompany();

    $scope.edit = function(){
        $scope.company.editing = true;
    };

    $scope.save = function(){
        var form = $scope.companyForm;
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('company.update').add('company.update',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('company.update').add('company.update',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('company.update', Company.updateHB($scope.company), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra företagsuppgifter',
            '4XX': 'Företagsuppgifter kunde inte ändras',
            '5XX': 'Ett fel inträffade. Företagsuppgifter kunde inte ändras. Försök igen senare.'
        })
        .then(function(res){
            $state.go($state.current, {}, {reload: true});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.undoEdit = function(){
        if(WaitLock.check())return;
        angular.copy($scope.companyDataBackup, $scope.company);
        $scope.company.editing = false;
        $scope.companyForm.$setPristine();
        $scope.companyForm.$setUntouched();
    };
}]);
