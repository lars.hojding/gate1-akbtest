angular.module('app.services')
.factory('Company', ['AuthHttp', function(AuthHttp){
    return {
        getAB: function(orgNr){
            return AuthHttp.get('company/AB', {params: {orgNr: orgNr}});
        },
        updateAB: function(company, invoiceAddress){
            return AuthHttp.put('company/AB', company).then(function(){
            	invoiceAddress.orgNr = company.orgNr;
            	return AuthHttp.put('company/invoiceaddress', invoiceAddress).then(function(){
            		
            	}, function(err){
            		if(err.status == 404){
            			return AuthHttp.post('company/invoiceaddress', invoiceAddress);
            		}
            	});
            });
        },
        getHB: function(orgNr){
            return AuthHttp.get('company/HB', {params: {orgNr: orgNr}});
        },
        updateHB: function(company){
            return AuthHttp.put('company/HB', company);
        },
        getShareinfo: function(orgNr){
            return AuthHttp.get('company/AB/shareinfo', {params: {orgNr: orgNr}});
        },
        getInvoiceAddress: function(orgNr){
        	return AuthHttp.get('company/invoiceaddress', {params: {orgNr: orgNr}});
        },
    };
}]);
