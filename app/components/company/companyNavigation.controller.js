angular.module('app.controllers')
.controller('CompanyNavigationCtrl', ['$scope', '$stateParams', 'currUser', 'companySubscription', function($scope, $stateParams, currUser, companySubscription){
    var getCompanyType = function(co){
        switch(co.companyType){
            case 1: return 'AB';
            case 3: return 'HB';
        }
        return 'AB';
    };
    $scope.orgNr = $stateParams.orgNr;
    $scope.companyName = companySubscription.companyName;
    $scope.companyType = getCompanyType(companySubscription);
    if(typeof currUser == 'object'){
    	$scope.isSystemUser = currUser.isSystemUser;
    }
}]);
