angular.module('app.controllers')
.controller('CompanyABCtrl', ['$scope', 'Company', '$stateParams', 'Feedback', '$state', 'WaitLock',
                function ($scope, Company, $stateParams, Feedback, $state, WaitLock){
    $scope.orgNr = $stateParams.orgNr;

    $scope.submitDisabled = false;

    var loadCompany = function(){
        Feedback.http('company.ab.get', Company.getAB($scope.orgNr), {
            '4XX': 'Information om företaget kunde inte hämtas.',
            '5XX': 'Ett fel inträffade. Information om företaget kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.companyDataBackup = angular.copy(res.data);
            $scope.company = res.data;
        });
    };
    var loadInvoiceAddress = function() {
    	Feedback.http('company.ab.get', Company.getInvoiceAddress($scope.orgNr), {
    		'5XX': 'Ett fel inträffade. Faktureringsadress kunde inte hämtas.'
    	})
    	.then(function(res) {
    		$scope.invoiceAddressDataBackup = angular.copy(res.data);
    		$scope.invoiceAddress = res.data;
    	});
    };
    loadCompany();
    loadInvoiceAddress();

    $scope.save = function(){
        var form = $scope.companyForm;
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('company.update').add('company.update',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('company.update').add('company.update',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('company.update', Company.updateAB($scope.company, $scope.invoiceAddress), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra företagsuppgifter',
            '4XX': 'Företagsuppgifter kunde inte ändras',
            '5XX': 'Ett fel inträffade. Företagsuppgifter kunde inte ändras. Försök igen senare.'
        })
        .then(function(res){
            $scope.company.editing = false;
            $state.go($state.current, {}, {reload: true});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.undoEdit = function(){
        if(WaitLock.check())return;
        angular.copy($scope.companyDataBackup, $scope.company);
        angular.copy($scope.invoiceAddressDataBackup, $scope.invoiceAddress);
        $scope.company.editing = false;
        $scope.companyForm.$setPristine();
        $scope.companyForm.$setUntouched();
    };
}]);
