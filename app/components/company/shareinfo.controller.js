angular.module('app.controllers')
.controller('ShareinfoCtrl', ['$scope', 'Company', '$stateParams', '$state', 'Feedback', 'WaitLock',
                function ($scope, Company, $stateParams, $state, Feedback, WaitLock){
    $scope.orgNr = $stateParams.orgNr;

    $scope.shares = [];
    var loadShareinfo = function(){
        Feedback.http('shareinfo.get', Company.getShareinfo($scope.orgNr), {
            '4XX': 'Information om företagets innehav kunde inte hämtas.',
            '5XX': 'Ett fel inträffade. Information om företagets innehav kunde inte hämtas. Försök igen senare.'
        })
        .then(function(res){
            $scope.shares = res.data;
        });
    };
    loadShareinfo();

}]);
