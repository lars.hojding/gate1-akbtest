angular.module('app.controllers')
.controller('NewTransactionCtrl', ['$scope', 'Transaction', '$stateParams', '$state', 'Feedback', 'WaitLock', 'Owner', 'Company', '$timeout',
            function ($scope, Transaction, $stateParams, $state, Feedback, WaitLock, Owner, Company, $timeout){
    var orgNr = $stateParams.orgNr;
    $scope.getInputOwnerId = function(){
    	if($stateParams.foreignOwner){
    		return $stateParams.foreignOwner.ownerId;
    	}
        if(typeof $stateParams.ownerId === 'undefined')return '';
        if(isNaN(parseInt($stateParams.ownerId)))return '';
        var a = parseInt($stateParams.ownerId);
        if(a > 0)return a;
        return '';
    };
    
    $scope.foreignOwner = {};
    if($stateParams.foreignOwner){
    	$scope.foreignOwner=$stateParams.foreignOwner;
    }
    
    $scope.transaction = {
      "transactionTypeId": 1,
      "aktieslag": "",
      "ownerId": $scope.getInputOwnerId(),
      "eventTypeId": 1,
      "fromNumberSerie": 0,
      "toNumberSerie": 0,
      "quantity": 0,
      "orderDate": "",
      "tradeDate": "",
      "transactionCharge": 0,
      "status": 0,
      "isPawned": false,
      "pawnee": "",
      "comment": "",
      "custodian": "",
      "dividendRight": true,
      "consentDisclaimers": false,
      "presaleDisclaimers": false,
      "preEmption": false,
      "conversionDisclaimers": false,
      "redemptionDisclaimers": false,
      "shareCertificatesIssued": false
    };
    
    $scope.hasValidOwner = false;
    $scope.checkingOwner = false;

    var loadShareTypes = function() {
    	if(WaitLock.lock()) return;
    	Feedback.http(
    		'transactions.create',
    		Company.getShareinfo(orgNr),
    		{
    			'4XX': "Kunde inte hämta bolagsdata. Kontrollera dina behörigheter.",
    			'5XX': "Ett fel har uppstått. Kunde inte hämta bolagsdata. Försök igen senare."
    			
    	})
    	.then(function(res){
    		$scope.shareTypes = res.data;
    		if(res.data.length > 0){
    			$scope.transaction.aktieslag = res.data[0].aktieSlag;
    		}
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('transactions.create')
                	.add('transactions.create', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		    		
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    	});
    };
    
    var checkOwnerExist = function(){
        $scope.checkingOwner = true;
        $timeout(function(){
            Owner.exists($scope.transaction.ownerId, orgNr)
            .then(function(exists){
                if(exists.exists){
                	$scope.transaction.ownerName = exists.owner.firstName + ' ' + exists.owner.lastName;
                    // Owner exists - show the rest of the form
                    $scope.checkingOwner = false;
                    $scope.hasValidOwner = true;
                }else{
                    // Owner does not exist - go to owner registration
                    $state.go('user.company.owner.newFromTransaction', {orgNr: orgNr, ownerId: $scope.transaction.ownerId});
                }
            });
        }, 30);
    };

    $scope.checkOwner = function(){
        var formOwnerId = $scope.transactionForm.ownerId;
        if(formOwnerId.$pristine || formOwnerId.$invalid){
            formOwnerId.$setDirty();
            formOwnerId.$setTouched();
            return;
        }
        checkOwnerExist();
        loadShareTypes();
    };
    
    $scope.checkForeignOwner = function(){
    	$scope.transaction.ownerId = $scope.foreignOwner.ownerId;
    	$scope.transaction.ownerName = $scope.foreignOwner.firstName + ' ' + $scope.foreignOwner.lastName;
    	$scope.checkingOwner = false;
    	$scope.hasValidOwner = true;
    	loadShareTypes();
    };
    
    if($scope.getInputOwnerId()){
        checkOwnerExist();
        loadShareTypes();
    };

    $scope.newForeignOwner = function(){
        $state.go('user.company.owner.newForeignOwnerFromTransaction', {orgNr: orgNr});
    };
    
    $scope.$watch('transaction.transactionTypeId', function(newValue, oldValue){
        if(newValue == 1){
            $scope.transaction.eventTypeId = 1;
            $scope.transaction.eventText = '';
            $scope.transactionForm.handelsetypText.$setPristine();
            $scope.transactionForm.handelsetypText.$setUntouched();
        }else{
            $scope.transaction.eventTypeId = 0;
        }
    });

    $scope.$watch('transaction.custodianYes', function(newValue, oldValue){
        if(newValue == false){
            $scope.transaction.custodian = '';
        }
    });

    $scope.$watch('transaction.eventTypeId', function(newValue, oldValue){
        if(newValue != -1){
            $scope.transaction.eventText = '';
            $scope.transactionForm.handelsetypText.$setPristine();
            $scope.transactionForm.handelsetypText.$setUntouched();
        }
    });

    $scope.$watch('transaction.eventText', function(newValue, oldValue){
        if(newValue !== ''){
            $scope.transaction.eventTypeId = -1;
        }
    });

    $scope.$watch('transaction.fromNumberSerie', function(newValue, oldValue){
    	if(newValue != oldValue){
    		if($scope.transaction.toNumberSerie - $scope.transaction.fromNumberSerie + 1 > 0){
    			$scope.transaction.quantity = $scope.transaction.toNumberSerie - $scope.transaction.fromNumberSerie + 1;
    		}
    		else {
    			$scope.transaction.quantity = 0;    			
    		}
    	}
    });
    
    $scope.$watch('transaction.toNumberSerie', function(newValue, oldValue){
    	if(newValue != oldValue){
    		if($scope.transaction.toNumberSerie - $scope.transaction.fromNumberSerie + 1 > 0){
    			$scope.transaction.quantity = $scope.transaction.toNumberSerie - $scope.transaction.fromNumberSerie + 1;
    		}
    		else {
    			$scope.transaction.quantity = 0;    			
    		}
    	}
    })
    
    $scope.save = function(){
        var _transaction = angular.copy($scope.transaction);
        _transaction.orgNr = orgNr;
        var form = $scope.transactionForm;
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('transactions.create').add('transactions.create',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll i alla obligatoriska fält');
            }else{
                Feedback.clearTarget('transactions.create').add('transactions.create',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(_transaction.tradeDate instanceof Date){
            _transaction.tradeDate.setHours(10);
            _transaction.tradeDate = _transaction.tradeDate.toISOString().substr(0,10);
        }
        if(typeof _transaction.tradeDate === 'string'){
            _transaction.tradeDate = _transaction.tradeDate.replace(/-/g, '');
        }
        if(WaitLock.lock())return;
        Feedback.http('transactions.create', Transaction.createTransaction(_transaction), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att lägga till aktiebokshändelse',
            '4XX': 'Aktiebokshändelse kunde inte läggas till',
            '5XX': 'Ett fel inträffade. Aktiebokshändelse kunde inte läggas till. Försök igen senare.'
        }).then(function(res){
            $state.go('user.company.transaction.listab', {orgNr: orgNr});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };
    
    if($scope.foreignOwner && $scope.foreignOwner.foreign){
    	$scope.checkForeignOwner();
    }
}]);
