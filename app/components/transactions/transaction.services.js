angular.module('app.services')
.factory('Transaction', ['AuthHttp', function(AuthHttp){
    return {
        getTransactions: function(orgNr){
            return AuthHttp.get('transaction/', {params: {orgNr: orgNr}});
        },
        createTransaction: function(transactionData){
            return AuthHttp.post('transaction/', transactionData);
        },
        cancelTransaction: function(transaction){
        	return AuthHttp.get('transaction/cancel', {params: {transactionId: transaction.transactionId, orgNr: transaction.orgNr}});
        }
    }
}]);
