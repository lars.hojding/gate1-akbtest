angular.module('app.controllers')
.controller('TransactionsCtrl', ['$scope', 'Transaction', 'WaitLock', '$stateParams', 'Feedback', '$q', 'Owner', '$state',
                            function ($scope, Transaction, WaitLock, $stateParams, Feedback, $q, Owner, $state){
    $scope.orgNr = $stateParams.orgNr;

    $scope.isAB = $state.current.name == 'user.company.transaction.listab';

    $scope.transactions = [];
    
    $scope.transactionsPerPage = 10;

    var loadTransactions = function(){
    	Feedback.http(
    		'transactions.get', 
    		Transaction.getTransactions($scope.orgNr), 
    		{
		        403: 'Ej behörig att se aktiebokshändelser',
		        '4XX': 'Information om aktiebokshändelser kunde inte hämtas',
		        '5XX': 'Ett fel inträffade. Information om aktiebokshändelser kunde inte hämtas. Försök igen senare.'
    	})
    	.then(function(res){
    		$scope.transactions = res.data;
    	});
    };
    loadTransactions();
    
    $scope.cancelTransaction = function(transaction){
    	if(WaitLock.lock()) return;
    	Feedback.http(
    		'transactions.get',
    		Transaction.cancelTransaction(transaction),
    		{
    			'4XX': 'Kunde inte makulera händelse.',
    			'5XX': 'Ett fel inträffade. Händelsen kunde inte makuleras. Försök igen senare.'
    		})
    	.then(function(res){
    		loadTransactions();
    	},
    	function(err){
        	if(err.errorCode === 'UNKNOWN_ERROR'){
        		Feedback.clearTarget('transactions.get')
                	.add('transactions.get', Feedback.types.ERROR, 500, 'Ett fel har inträffat. Försök igen.');
        	}    		    		    		
    	})
    	['finally'](function(){
    		WaitLock.unlock();
    	});
    };
}]);
