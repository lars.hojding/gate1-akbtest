angular.module('app.controllers')
.controller('SubscriptionCtrl', ['$scope', 'Subscriptions', '$stateParams', 'Feedback', '$state', 'WaitLock',
            function($scope, Subscriptions, $stateParams, Feedback, $state, WaitLock){
    $scope.subscription = {};
    $scope.subscriptionOrig = {};
    $scope.orgNr = $stateParams.orgNr;
    Feedback.http('subscriptions.single.get', Subscriptions.getCompanySubscription($scope.orgNr), {
        403: 'Ej behörig att se prenumeration',
        '4XX': 'Prenumeration kunde inte visas',
        '5XX': 'Ett fel inträffade. Prenumeration kunde inte visas. Försök igen senare.'
    })
    .then(function(res){
        $scope.subscriptionOrig = angular.copy(res.data);
        $scope.subscription = res.data;
    });

    $scope.updateSubscription = function(subscription, form){
        if(form.$invalid){
            angular.forEach(form.$error, function(ve){
                angular.forEach(ve, function(el){
                    el.$setDirty();
                    el.$setTouched();
                });
            });
            if(typeof form.$error.required !== 'undefined'){
                Feedback.clearTarget('subscriptions.update').add('subscriptions.update',
                    Feedback.types.FAILURE,
                    400,
                    'Fyll alla fält');
            }else{
                Feedback.clearTarget('subscriptions.update').add('subscriptions.update',
                    Feedback.types.FAILURE,
                    400,
                    'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.');
            }
            return;
        }
        if(WaitLock.lock())return;
        Feedback.http('subscriptions.update', Subscriptions.updateSubscription(subscription), {
            400: 'Felaktiga uppgifter. Kontrollera uppgifterna och försök igen.',
            403: 'Ej behörig att ändra på prenumeration',
            '4XX': 'Prenumeration kunde inte ändras',
            '5XX': 'Ett fel inträffade. Prenumeration kunde inte ändras. Försök igen senare.'
        })
        .then(function(res){
            subscription.editing = false;
            $state.go($state.current, {}, {reload: true});
        })
        ['finally'](function(){
            WaitLock.unlock();
        });
    };

    $scope.undoEdit = function(subscription, form){
        if(WaitLock.check())return;
        angular.copy($scope.subscriptionOrig, $scope.subscription);
        $scope.subscription.editing = false;
        form.$setPristine();
        form.$setUntouched();
    };
}]);
