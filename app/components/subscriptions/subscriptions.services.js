angular.module('app.services')
.factory('Subscriptions', ['AuthHttp', 'Authentication', '$q', function(AuthHttp, Authentication, $q){
    return {
        getUserSubscriptions: function(orgNr, userId){
            var deferred = $q.defer();
            Authentication.getAuthData()
            .then(function(resAuth){
                var isUserIdSet = function(uid){
                    if(typeof uid === 'undefined')return false;
                    if(uid === null)return false;
                    return true;
                };
                var params = {};
                if(isUserIdSet(userId)){
                    params.userId = userId;
                }else{
                    params.userId = resAuth.data.uid;
                }
                if(orgNr != null){
                	params.orgNr = orgNr;
                }
                AuthHttp.get('subscription/user', {params: params})
                .then(function(resGet){
                    deferred.resolve(resGet);
                }, function(err){
                    deferred.reject(err);
                });
            }, function(err){
                deferred.reject(err);
            });
            return deferred.promise;
        },
        getCompanySubscription: function(orgNr){
            return AuthHttp.get('subscription/company', {params: {orgNr: orgNr}});
        },
        createSubscription: function(formData){
            return AuthHttp.post('subscription', formData);
        },
        updateSubscription: function(formData){
            return AuthHttp.put('subscription', formData);
        },
        getCompanyType: function(orgNr){
            return this.getCompanySubscription(orgNr)
            .then(function(res){
                switch(res.data.companyType){
                    case 1: return 'AB';
                    case 3: return 'HB';
                }
                return 'AB';
            });
        }
    };
}]);
