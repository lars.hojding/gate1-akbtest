angular.module('app.services')
.factory('WaitLock', ['$rootScope', function($rootScope){
    var anonymous = '___anonymousTarget';
    var fixTarget = function(target){
        return typeof target !== 'string' ? anonymous : target;
    };
    var waitLock = {
        lockedTargets: {},
        check: function(target){
            target = fixTarget(target);
            return typeof this.lockedTargets[target] != 'undefined' ? true : false;
        },
        lock: function(target){
            var res = this.check(target);
            target = fixTarget(target);
            this.lockedTargets[target] = true;
            return res;
        },
        unlock: function(target){
            target = fixTarget(target);
            if(this.check(target)){
                delete this.lockedTargets[target];
            }
        },
        unlockAll: function(){
            this.lockedTargets = {};
        }
    };
    $rootScope.$on('$stateChangeStart', function(evt, toState){
        waitLock.unlockAll();
    });
    return waitLock;
}]);
