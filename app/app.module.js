'use strict';

// Declare app level module which depends on views, and components
angular.module('app.controllers', ['app.services']);
angular.module('app.directives', []);
angular.module('app.services', []);
angular.module('app.filters', []);



var appDependencies = [
        'app.controllers',
        'app.directives',
        'app.services',
        'app.filters',
        'ngMessages',
        'ui.router',
        'ngCookies',
        'angularUtils.directives.dirPagination'
    ];

angular.module('app', appDependencies)
.config(['$httpProvider', '$compileProvider',
            function($httpProvider, $compileProvider) {
    $httpProvider.defaults.withCredentials = true;

    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    // Allow BankID
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|bankid):/);
}]);


angular.module('app')
.run(['$rootScope', '$state', '$stateParams', '$urlRouter', 'Authentication', '$http',
                function($rootScope, $state, $stateParams, $urlRouter, Authentication, $http) {
        $rootScope.restUrl = '';

        $rootScope.$on('$stateChangeStart', function(evt, toState){
            if(toState.name === 'anon'){
                if(Authentication.getAuthDataBlocking().status === 200){
                    evt.preventDefault();
                    $state.go('user.dashboard');

                }
            }
            else if(toState.name === 'federation.login'){
                $cookieStore.remove('uid');
                $cookieStore.remove('token');
                $state.go('anon',
                    {orgnr: $state.params.orgnr, 
            		email: $state.params.email, 
            		phone: $state.params.phone,
            		firstName: $state.params.firstName,
            		lastName: $state.params.lastName,
            		clientType: $state.params.clientType
            	});
            }
        });
    }
]);

// Clear all feedback messages on state change.
angular.module('app')
.run(['$rootScope', 'Feedback', function($rootScope, Feedback) {
        $rootScope.$on('$stateChangeStart', function(evt, toState){
            Feedback.clearAll();
        });
    }
]);
