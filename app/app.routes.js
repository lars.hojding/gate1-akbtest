angular.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/');

    // IF NOT LOGGED IN
    $stateProvider
    .state('anon', {
        url: '/?orgnr&email&phone&firstName&lastName&clientType&grandidsession',
        abstract: false,
        templateUrl: 'components/login/loginUser.view.html',
        controller: 'LogInUserCtrl as loginUser',
        params:{orgnr: null, email: null, phone: null, firstName: null, lastName: null, clientType: null, grandidsession: null}
    })

    .state('login', {
    	url:'/login?orgnr&email&phone&firstName&lastName&clientType',
    	abstract : false,
    	templateUrl: 'components/login/loginUser.view.html',
    	controller: 'LogInUserCtrl as loginUser',
        params:{orgnr: null, email: null, phone: null, firstName: null, lastName: null, clientType: null}
    })
    
    // IF LOGGED IN
    .state('user', {
        abstract: true,
        templateUrl: 'components/user/user.view.html',
        controller: 'UserCtrl as user',
        resolve:{
        	'currUser' : ['UserHandler', function(UserHandler){
                return UserHandler.getSelf().then(function(res){
                    return res.data;
                });
        	}]
        }
    })
    .state('user.dashboard', {
        url: '/dashboard',
        templateUrl: 'components/userDashboard/userDashboard.view.html',
        controller: 'UserDashboardCtrl as userDashboard',
        params: {orgnr: null, email: null, phone: null, firstName: null, lastName: null, clientType: null}
    })
    .state('user.settings', {
        url: '/settings',
        templateUrl: 'components/userSettings/userSettings.view.html',
        controller: 'UserSettingsCtrl as userSettings',
        resolve:{}
    })

    //Administration
    .state('user.administration', {
    	url: '/administration',
    	template: '<ui-view/>',
    	abstract: true
    })
    
    .state('user.administration.main', {
    	url: '',
    	templateUrl: 'components/administration/administration.view.html',
    	controller: 'AdminCtrl as admin'
    })
    
    .state('user.administration.users',{
    	url: '/users',
    	templateUrl: 'components/administration/user/userAdministration.view.html',
    	controller: 'UserAdminCtrl as userAdmin'
    })
    
    .state('user.administration.users.update',{
    	url: '/update',
    	templateUrl: 'components/administration/user/userUpdate.view.html',
    	controller: 'UserUpdateCtrl as userAdminUpdate'
    })
    
    .state('user.administration.resellers',{
    	url: '/resellers',
    	templateUrl: 'components/administration/reseller/resellerAdministration.view.html',
    	controller: 'ResellerAdminCtrl as resellerAdmin'
    })
    
    .state('user.administration.resellers.new', {
        url: '/new',
        templateUrl: 'components/administration/reseller/resellerNew.view.html',
        controller: 'ResellerNewCtrl as resellerAdminNew'
    })
    
    .state('user.administration.resellers.update', {
    	url: '/update',
    	templateUrl: 'components/administration/reseller/resellerUpdate.view.html',
    	controller: 'ResellerUpdateCtrl as resellerAdminUpdate'
    })

    .state('user.administration.invoicing', {
    	url: '/invoicing',
    	templateUrl: 'components/administration/invoicing/invoicingAdministration.view.html',
    	controller: 'InvoicingAdminCtrl as invoicingAdmin'
    })
    
    .state('user.administration.invoicing.renewalReview', {
    	url: '/renewalReview',
    	views: {
    		'invoicingContent' : {
		    	templateUrl: 'components/administration/invoicing/renewalReview.view.html',
		    	controller: 'ReviewCtrl'
    		}
    	}
    })
    
    .state('user.administration.invoicing.newInvoices', {
    	url: '/new',
    	views: {
    		'invoicingContent' : {
		    	templateUrl: 'components/administration/invoicing/newInvoices.view.html',
		    	controller: 'ReviewCtrl'
    		}
    	}
    })
    
    .state('user.administration.invoicing.renewalInvoices', {
    	url: '/renew',
    	views: {
    		'invoicingContent' : {
		    	templateUrl: 'components/administration/invoicing/renewalInvoices.view.html',
		    	controller: 'ReviewCtrl'
    		}
    	}
    })
    
    .state('user.administration.subscription', {
    	url: '/subscription/:orgNr',
    	template: '<ui-view/>',
    	abstract: true
    })

    .state('user.administration.subscription.delete', {
    	url: '/delete',
    	templateUrl: 'components/administration/subscription/deleteSubscription.view.html',
    	controller: 'SubscriptionAdminCtrl as subscriptionAdmin'
    })
    
    .state('user.administration.companygroup', {
    	url: '/companygroup',
    	templateUrl: 'components/administration/companygroupregister/companygroupAdmin.view.html',
    	controller: 'CompanygroupAdminCtrl as companygroupAdmin'
    })
    
    .state('user.help', {
        url: '/help',
        template: '<ui-view/>',
        abstract: true
    })
    .state('user.help.main', {
        url: '',
        templateUrl: 'components/help/main.view.html',
        controller: 'HelpCtrl'
    })


    // Subscriptions
    .state('user.subscriptions', {
        url: '/subscriptions',
        template: '<ui-view/>',
        abstract: true
    })


    // Add new subscription/company
    .state('user.registration', {
        url: '/new',
        templateUrl: 'components/registration/registration.view.html',
        controller: 'RegistrationCtrl',
        params: {basicData: null, email: null, phone: null, firstName: null, lastName: null, clientType: null, termsAccepted: null}
    })


    // Company
    .state('user.company', {
        url: '/company/:orgNr',
        templateUrl: 'components/company/company.view.html',
        controller: 'CompanyNavigationCtrl',
        resolve: {
            'companySubscription': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
                return Subscriptions.getCompanySubscription($stateParams.orgNr).then(function(res){
                    return res.data;
                });
            }]
        }
    })


    // Hijacking list
    .state('user.company.hijackinglist', {
        views: {
            'companycontent@user.company': {
                template: '<ui-view/>',
                abstract: true
            }
        }
    })
    .state('user.company.hijackinglist.entries', {
        url: '/hijackinglist/',
        templateUrl: 'components/hijackinglist/hijackinglist.view.html',
        controller: 'HijackinglistCtrl'
    })
    .state('user.company.hijackinglist.new', {
        url: '/hijackinglist/new',
        templateUrl: 'components/hijackinglist/hijackinglistNew.view.html',
        controller: 'HijackinglistNewCtrl'
    })

    // Delegates
    .state('user.company.delegates', {
        views: {
            'companycontent@user.company': {
                template: '<ui-view/>',
                abstract: true
            }
        }
    })
    .state('user.company.delegates.entries', {
        url: '/delegates/',
        templateUrl: 'components/delegates/delegates.view.html',
        controller: 'DelegatesCtrl'
    })
    .state('user.company.delegates.new', {
        url: '/delegate/',
        templateUrl: 'components/delegates/newDelegate.view.html',
        controller: 'NewDelegateCtrl'
    })

    // Subscription
    .state('user.company.subscription', {
        url: '/subscription',
        views: {
            'companycontent': {
                templateUrl: 'components/subscriptions/subscription.view.html',
                controller: 'SubscriptionCtrl'
            }
        }
    })


    // Owner
    .state('user.company.owner', {
        views: {
            'companycontent@user.company': {
                template: '<ui-view/>',
                abstract: true
            }
        }
    })
    .state('user.company.owner.newFromTransaction', {
        url: '/owner/new/t/:ownerId',
        templateUrl: 'components/owner/owner.view.html',
        controller: 'NewOwnerCtrl',
        resolve: {
            'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
                return Subscriptions.getCompanyType($stateParams.orgNr);
            }]
        }
    })
    .state('user.company.owner.newFromHolding', {
        url: '/owner/new/h/:ownerId',
        templateUrl: 'components/owner/owner.view.html',
        controller: 'NewOwnerCtrl',
        resolve: {
            'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
                return Subscriptions.getCompanyType($stateParams.orgNr);
            }]
        }
    })
    .state('user.company.owner.newForeignOwnerFromTransaction', {
    	url: '/owner/new/foreign/t',
    	templateUrl: 'components/owner/newForeignOwner.view.html',
    	controller: 'NewOwnerCtrl',
    	resolve: {
    		'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
    			return Subscriptions.getCompanyType($stateParams.orgNr);
    		}]
    	}
    })
    .state('user.company.owner.newForeignOwnerFromHolding', {
    	url: '/owner/new/foreign/h',
    	templateUrl: 'components/owner/newForeignOwner.view.html',
    	controller: 'NewOwnerCtrl',
    	resolve: {
    		'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
    			return Subscriptions.getCompanyType($stateParams.orgNr);
    		}]
    	}
    })

    // Holding
    .state('user.company.holding', {
        views: {
            'companycontent@user.company': {
                template: '<div ui-view="holding"></div><div ui-view="shareinfo"></div>',
                abstract: true
            }
        }
    })
    .state('user.company.holding.listab', {
        url: '/AB/holdings',
        views: {
            'holding': {
                templateUrl: 'components/holding/holdingsAB.view.html',
                controller: 'HoldingsCtrl'
            }
        },
        resolve: {
            'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
                return Subscriptions.getCompanyType($stateParams.orgNr);
            }]
        }
    })
    .state('user.company.holding.listhb', {
        url: '/HB/holdings',
        views: {
            'holding': {
                templateUrl: 'components/holding/holdingsHB.view.html',
                controller: 'HoldingsCtrl'
            }
        },
        resolve: {
            'companyType': ['$stateParams', 'Subscriptions', function($stateParams, Subscriptions){
                return Subscriptions.getCompanyType($stateParams.orgNr);
            }]
        }
    })
    .state('user.company.holding.new', {
        url: '/HB/holding/new/',
        views: {
            'holding': {
                templateUrl: 'components/holding/newHolding.view.html',
                controller: 'NewHoldingCtrl'
            }
        }
    })
    .state('user.company.holding.newWithOwner', {
        url: '/HB/holding/new/:ownerId',
        views: {
            'holding': {
                templateUrl: 'components/holding/newHolding.view.html',
                controller: 'NewHoldingCtrl'
            }
        },
        params:{foreignOwner: null}
    })

    // Transactions
    .state('user.company.transaction', {
        views: {
            'companycontent@user.company': {
                template: '<ui-view/>',
                abstract: true
            }
        }
    })
    .state('user.company.transaction.listab', {
        url: '/AB/transactions',
        templateUrl: 'components/transactions/transactions.view.html',
        controller: 'TransactionsCtrl'
    })
    .state('user.company.transaction.new', {
        url: '/transaction/new',
        templateUrl: 'components/transactions/newTransaction.view.html',
        controller: 'NewTransactionCtrl'
    })
    .state('user.company.transaction.newWithOwner', {
        url: '/transaction/new/:ownerId',
        templateUrl: 'components/transactions/newTransaction.view.html',
        controller: 'NewTransactionCtrl',
        params: {foreignOwner: null}
    })


    .state('user.company.report', {
        url: '/report',
        views: {
            'companycontent': {
                templateUrl: 'components/report/report.view.html',
                controller: 'ReportCtrl'
            }
        },
        resolve: {
            'holdings': ['$stateParams', 'Holding', function($stateParams, Holding){
                return Holding.ab.getHoldings($stateParams.orgNr).then(function(res){
                	return res.data;
                })
            }]
        }
    })


    // Company main page
    .state('user.company.company', {
        views: {
            'companycontent@user.company': {
                templateUrl: 'components/company/companyContent.view.html',
                abstract: true
            }
        }
    })
    // AB
    .state('user.company.company.ab', {
        url: '/AB',
        views: {
            'company': {
                templateUrl: 'components/company/companyAB.view.html',
                controller: 'CompanyABCtrl',
                resolve:{}
            },
            'shareinfo': {
                templateUrl: 'components/company/shareinfo.view.html',
                controller: 'ShareinfoCtrl'
            },
            'subscription': {
                templateUrl: 'components/subscriptions/subscription.view.html',
                controller: 'SubscriptionCtrl'
            }
        }
    })
    // HB
    .state('user.company.company.hb', {
        url: '/HB/',
        views: {
            'company': {
                templateUrl: 'components/company/companyHB.view.html',
                controller: 'CompanyHBCtrl',
                resolve:{}
            },
            'subscription': {
                 templateUrl: 'components/subscriptions/subscription.view.html',
                 controller: 'SubscriptionCtrl'
            }
        }
    })


}]);
