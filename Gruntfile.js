module.exports = function(grunt) {
    require( "matchdep" ).filterDev( "grunt-*" ).forEach( grunt.loadNpmTasks );
    grunt.loadNpmTasks('assemble-less');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            development:{
                options: {
                    paths: ['app/assets/css'],
                    imports: {
                        reference: ['variables.less']
                    }
                },
                files: {
                    "app/assets/css/style.css": [ 'app/assets/css/main.less', 'app/components/**/*.less']
                }
            }
        },

        cssmin: {
            target: {
                files: {
                    'app/assets/css/style.min.css': ['app/assets/css/style.css']
                }
            }
        },

        uglify: {
            options:{
                mangle:false,
                beautify: true
            },

            app: {
                files: {
                    'app/app.min.js': ['app/app.module.js', 'app/app.routes.js','app/components/**/*.controller.js', 'app/components/**/*.directive.js']
                }
            },
            services: {
                files:{
                    'app/services.min.js': ['app/components/**/*.services.js']
                }
            },
            controllers: {
                files: {
                    'app/controllers.min.js' : ['app/components/**/*.controller.js']
                }
            }
        },

        clean: {
            build: ['build/*.html', 'build/*.min.js', 'build/components', 'build/assets']
        },

        copy: {
            components: {
                files: [{
                    expand: true,
                    cwd: 'app/components/',
                    src: ['**/*.html'],
                    dest: 'build/components'
                }]
            },
            assets: {
                files: [{
                    expand: true,
                    cwd: 'app/assets/',
                    src: ['bower_components/**', 'img/**', '*.pdf', '**/*.min.css'],
                    dest: 'build/assets'
                }]
            },
            approot: {
                files: [{
                    expand: true,
                    cwd: 'app/',
                    src: ['*.html', '*.min.js'],
                    dest: 'build/'
                }]
		    }
        },

        watch: {
            build: {
                files: ['app/**/*'],
                tasks: ['build']
            }
        }
    })

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('buildnocopy', ['uglify', 'less', 'cssmin']);
    grunt.registerTask('build', ['clean:build', 'buildnocopy', 'copy']);
}
